
exports.init = function(port)
{
	const 	dgram 			= require('dgram'); 
	const 	server 			= dgram.createSocket("udp4"); 
	let 	PORT 			= 8001;
	let 	BROADCAST_ADDR 	= "192.168.0.255";


	server.bind(function() {
	    server.setBroadcast(true);
	    setInterval(broadcastNew, 3000);
	});

	function broadcastNew() {
	    var message = new Buffer("Broadcast message!");
	    server.send(message, 0, message.length, PORT, BROADCAST_ADDR, function() {
	        //console.log("Sent '" + message + "'");
	    });
	}
}