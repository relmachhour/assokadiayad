var listJs = [
                'public/lib/jquery/dist/jquery.min.js',
                'public/lib/jquery-mousewheel/jquery.mousewheel.min.js',
                'public/lib/moment/min/moment.min.js',
                'public/lib/lodash/lodash.min.js',
                'public/lib/jquery-ui/jquery-ui.js',
                'public/style/myjs/jquery.ui.widget.js',
                'public/lib/jquery.easing/js/jquery.easing.js',
                'public/lib/bootstrap/dist/js/bootstrap.min.js',
                'public/lib/bootstrap-select/js/bootstrap-select.js',
                'public/lib/remarkable-bootstrap-notify/dist/bootstrap-notify.js', // bootstrap-notify
                'public/lib/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min.js',


                'public/lib/fullcalendar/dist/fullcalendar.js',
                'public/lib/fullcalendar/dist/gcal.js',


                'public/lib/jquery.splitter/js/jquery.splitter.js', 





                'public/lib/angular/angular.js',
                'public/lib/angular-resource/angular-resource.js',
                'public/lib/angular-cookies/angular-cookies.js',
                'public/lib/angular-animate/angular-animate.js',
                'public/lib/angular-touch/angular-touch.js',
                'public/lib/angular-sanitize/angular-sanitize.js',
                'public/lib/angular-ui-router/release/angular-ui-router.js',
                'public/lib/angular-ui-utils/ui-utils.js',
                'public/lib/angular-bootstrap/ui-bootstrap-tpls.js',
                'public/lib/angular-ui-router.stateHelper/statehelper.min.js',
                'public/lib/angular-moment/angular-moment.js',
                'public/lib/angular-file-upload/dist/angular-file-upload.min.js',
                'public/lib/angular-click-outside/clickoutside.directive.js',

                'public/scripts/datetimepicker_ui.js',




                'public/lib/angular-socket-io/mock/socket-io.js',
                'public/lib/angular-socket-io/socket.min.js',


                'public/scripts/angular-bootstrap-calendar-tpls-modif.js',
                'public/scripts/moment-fr.js',

                'public/lib/ocModal/dist/ocModal.min.js', 




                'public/lib/angular-ui-calendar/src/calendar.js',

                'public/lib/angular-simple-logger/dist/angular-simple-logger.js',
                'public/lib/angular-google-maps/dist/angular-google-maps.js',




                'public/lib/Chart.js/Chart.js',
                'public/lib/angular-chart.js/dist/angular-chart.min.js',


                'public/scripts/jquery.dropdown.js',
                'public/scripts/angular-locale_fr-fr.js',
                'public/scripts/main.js',
                'public/scripts/jquery-layout-designer.js',
                'public/scripts/angular-ui-task-programmer.js',
                'public/scripts/imgViewer.js']


var UglifyJS = require('uglify-js')
var result = UglifyJS.minify('public/lib/jquery/dist/jquery.min.js', 'public/lib/jquery-mousewheel/jquery.mousewheel.min.js', 'public/lib/moment/min/moment.min.js', 'public/lib/lodash/lodash.min.js', 'public/lib/jquery-ui/jquery-ui.js', 'public/style/myjs/jquery.ui.widget.js', 'public/lib/jquery.easing/js/jquery.easing.js', 'public/lib/bootstrap/dist/js/bootstrap.min.js', 'public/lib/bootstrap-select/js/bootstrap-select.js', 'public/lib/remarkable-bootstrap-notify/dist/bootstrap-notify.js', 'public/lib/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min.js', 'public/lib/fullcalendar/dist/fullcalendar.js', 'public/lib/fullcalendar/dist/gcal.js', 'public/lib/jquery.splitter/js/jquery.splitter.js', 'public/lib/angular/angular.js', 'public/lib/angular-resource/angular-resource.js', 'public/lib/angular-cookies/angular-cookies.js', 'public/lib/angular-animate/angular-animate.js', 'public/lib/angular-touch/angular-touch.js', 'public/lib/angular-sanitize/angular-sanitize.js', 'public/lib/angular-ui-router/release/angular-ui-router.js', 'public/lib/angular-ui-utils/ui-utils.js', 'public/lib/angular-bootstrap/ui-bootstrap-tpls.js', 'public/lib/angular-ui-router.stateHelper/statehelper.min.js', 'public/lib/angular-moment/angular-moment.js', 'public/lib/angular-file-upload/dist/angular-file-upload.min.js', 'public/lib/angular-click-outside/clickoutside.directive.js', 'public/scripts/datetimepicker_ui.js', 'public/lib/angular-socket-io/mock/socket-io.js', 'public/lib/angular-socket-io/socket.min.js', 'public/scripts/angular-bootstrap-calendar-tpls-modif.js', 'public/scripts/moment-fr.js', 'public/lib/ocModal/dist/ocModal.min.js', 'public/lib/angular-ui-calendar/src/calendar.js', 'public/lib/Chart.js/Chart.js', 'public/lib/angular-chart.js/dist/angular-chart.min.js', 'public/scripts/angular-locale_fr-fr.js', 'public/scripts/main.js', 'public/scripts/jquery-layout-designer.js', 'public/scripts/angular-ui-task-programmer.js', {
  outSourceMap: "out.js.map",
  outFileName: "out.js"
});
console.log(result.)