'use strict';
/**
 * Module dependencies.
 */


const 	path 		= require("path"),
		init 		= require('./config/init')(),
		config 		= require('./config/config'),
		db 			= require('./config/sequelize/sequelize-connect'),
		chalk 		= require('chalk');





// Bootstrap db connection
//var db = mongoose.connect(config.db, function(err) {



// Init the express application
var app = require('./config/express')(db);

// Bootstrap passport config
//require('./config/passport')();

// Start the app by listening on <port>
 //app.listen(config.port); // replace line for socket
 app.get('server').listen(config.port);
 //console.log(config.port);
// Expose app
exports = module.exports = app;

// Logging initialization
console.log('Esvishow application started on port ' + config.port);

//require('./udpserver').init(config.port);