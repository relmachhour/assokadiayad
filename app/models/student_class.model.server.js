"use strict";

module.exports = function(sequelize, DataTypes) { 

  var student_class = sequelize.define('student_class', { 
        
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
    studentId: { 
      type: DataTypes.INTEGER,
      unique: false,
      primaryKey: false,
      field: 'student_id'
    },
    classId: { 
      type: DataTypes.INTEGER,
      unique: false,
      primaryKey: false,
      field: 'class_id'
    }
   }, { 
    underscored: true,
    freezeTableName: true,
    timestamps: true,
    paranoid:true,
    tableName: 'student_class',
    associate: function(models) { 
    }
   });
  return student_class;
 };