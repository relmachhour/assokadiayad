"use strict";

module.exports = function(sequelize, DataTypes) { 

  var TypeIncome = sequelize.define('typeIncome', {     

    name: { 
      type: DataTypes.STRING,
      field:'name'
    },
    fromStudent: { 
      type: DataTypes.BOOLEAN,
      field:'from_student'
    }  

   }, { 
    underscored: true,
    freezeTableName: true,
    timestamps: true,
    paranoid:true,
    tableName: 'type_incomes',
    associate: function(models) { 
      TypeIncome.hasMany(models.income);
    }
   });
  return TypeIncome;
 };
