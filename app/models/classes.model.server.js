"use strict";

module.exports = function(sequelize, DataTypes) { 

  var Class = sequelize.define('class', {     

    reference: { 
      type: DataTypes.STRING,
      field:'reference'
    },
    name: { 
      type: DataTypes.STRING,
      field:'name'
    }

   }, { 
    underscored: true,
    freezeTableName: true,
    timestamps: true,
    paranoid:true,
    tableName: 'class',
    associate: function(models) { 
      Class.belongsToMany(models.student, { through: 'student_class', foreignKey: 'classId', unique: false})

    }
   });
  return Class;
 };
