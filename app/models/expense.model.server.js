"use strict";

module.exports = function(sequelize, DataTypes) { 

  var Expense = sequelize.define('expense', {     

    savedAt: { 
      type: DataTypes.DATEONLY,
      defaultValue: DataTypes.NOW,
      field: 'saved_at'
    },    
    operation: { 
      type: DataTypes.STRING,
      field:'operation'
    },
    project: { 
      type: DataTypes.STRING,
      field:'project'
    },
    document: { 
      type: DataTypes.STRING,
      field:'document'
    },
    typePaiement: { 
      type: DataTypes.STRING,
      field:'type_paiement'
    },  
    amount: { 
      type: DataTypes.FLOAT,
      field:'amount'
    }

   }, { 
    underscored: true,
    freezeTableName: true,
    timestamps: true,
    paranoid:true,
    tableName: 'expenses',
    associate: function(models) { 
        Expense.belongsTo(models.typeExpense);
        //Expense.belongsTo(models.fund);
        //Expense.belongsTo(models.bank);
    }
   });
  return Expense;
 };
