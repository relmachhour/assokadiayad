"use strict";

module.exports = function(sequelize, DataTypes) { 

  var TypeExpense = sequelize.define('typeExpense', {     

    name: { 
      type: DataTypes.STRING,
      field:'name'
    }  

   }, { 
    underscored: true,
    freezeTableName: true,
    timestamps: true,
    paranoid:true,
    tableName: 'type_expenses',
    associate: function(models) { 
      TypeExpense.hasMany(models.expense);
    }
   });
  return TypeExpense;
 };
