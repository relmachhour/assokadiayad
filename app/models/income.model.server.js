"use strict";

module.exports = function(sequelize, DataTypes) { 

  var Income = sequelize.define('income', {     

    savedAt: { 
      type: DataTypes.DATE,
      defaultValue: DataTypes.NOW,
      field: 'saved_at'
    },    
    operation: { 
      type: DataTypes.STRING,
      field:'operation'
    },
    project: { 
      type: DataTypes.STRING,
      field:'project'
    },
    document: { 
      type: DataTypes.STRING,
      field:'document'
    },
    typePaiement: { 
      type: DataTypes.STRING,
      field:'type_paiement'
    },
    amount: { 
      type: DataTypes.FLOAT,
      field:'amount'
    },
    participationDate: { 
      type: DataTypes.DATEONLY,
      defaultValue:null,
      field:'participation_date'
    }

   }, { 
    underscored: true,
    freezeTableName: true,
    timestamps: true,
    paranoid:true,
    tableName: 'incomes',
    associate: function(models) { 
        Income.belongsTo(models.typeIncome);
        Income.belongsTo(models.student);
        //Income.belongsTo(models.fund);
        //Income.belongsTo(models.bank);
    }
   });
  return Income;
 };
