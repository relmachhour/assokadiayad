"use strict";

module.exports = function(sequelize, DataTypes) { 

  var Student = sequelize.define('student', {     

    reference: { 
      type: DataTypes.STRING,
      field:'reference'
    },
    firstName: { 
      type: DataTypes.STRING,
      field:'first_name'
    },
    lastName: { 
      type: DataTypes.STRING,
      field:'last_name'
    },
    displayName: { 
      type: DataTypes.STRING,
      field:'display_name'
    },
    birthDate: { 
      type: DataTypes.DATEONLY,
      field:'birth_date'
    },
    code: { 
      type: DataTypes.STRING,
      field:'code'
    },
    address: { 
      type: DataTypes.STRING
    },    
    city: { 
      type: DataTypes.STRING
    },
    phone: { 
      type: DataTypes.STRING
    }    

   }, { 
    underscored: true,
    freezeTableName: true,
    timestamps: true,
    paranoid:true,
    tableName: 'students',
    associate: function(models) { 
      Student.belongsToMany(models.class, { through: 'student_class', foreignKey: 'studentId', unique: false})
      Student.hasMany(models.income); 
    }
   });
  return Student;
 };
