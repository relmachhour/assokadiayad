'use strict';

/**
 * User Model
 */

var crypto = require('crypto');

/**
 * A Validation function for local strategy properties
 */
var validateLocalStrategyProperty = function(property) {
  if (((this.provider !== 'local' && !this.updated) || property.length !== 0) === false) {
    throw new Error('Local strategy failed');
  }
};

/**
 * A Validation function for local strategy password
 */
var validateLocalStrategyPassword = function(password) {
  if ((this.provider !== 'local' || (password && password.length > 6)) === false) {
    throw new Error('One field is missing');
  }
};

module.exports = function(sequelize, DataTypes) {
  var User = sequelize.define('user', {
    
    firstName: {
      type: DataTypes.STRING,
      defaultValue: '',
      field: 'firstname'
    },
    lastName: {
      type: DataTypes.STRING,
      field: 'lastname',
      defaultValue: ''
    },
    displayName: {
      type: DataTypes.STRING,
      field: 'displayname'
    },
    username: {
      type: DataTypes.STRING,
      unique: true
    },
    email: {
      type: DataTypes.STRING,
      unique: true
    },
    profileImageURL: {
      type:DataTypes.STRING,
      field: 'profile_mageURL'
    },
    roles: {
      type: DataTypes.STRING,
      defaultValue: 'ROLE_USER'
    },
    password: {
      type: DataTypes.STRING,
    },
    locked: {
      type: DataTypes.BOOLEAN,
      field: 'locked',
      defaultValue: false
    },
    provider: DataTypes.STRING,
    salt: DataTypes.STRING,
    token: DataTypes.STRING,
	  expires: DataTypes.DATE,
    resetPasswordToken: {
      type:DataTypes.STRING,
      field: 'reset_password_token'
    },
    resetPasswordExpires: {
      type:DataTypes.DATE,
      field: 'reset_password_expires'
    }
  }, 
  {
    underscored: true,
    freezeTableName: true,
    timestamps: true,
    paranoid:true,
    tableName: 'users',

    instanceMethods: {
      makeSalt: function() {
        return crypto.randomBytes(16).toString('base64');
      },
      authenticate: function(plainText) {
        return this.encryptPassword(plainText, this.salt) === this.hashedPassword;
      },
      encryptPassword: function(password, salt) {
        if (!password || !salt)
          return '';
        salt = new Buffer(salt, 'base64');
        return crypto.pbkdf2Sync(password, salt, 10000, 64).toString('base64');
      }
    },
    classMethods: {
      findUniqueUsername: function(username, suffix, callback) {
        var _this = this;
        var possibleUsername = username + (suffix || '');

        _this.find({
          where: {
            username: possibleUsername
          }
        }).then(function(user) {
          if (!user) {
            callback(possibleUsername);
          } else {
            return _this.findUniqueUsername(username, (suffix || 0) + 1, callback);
          }
        });
      }
    },
    associate: function(models) {
       //User.hasMany(models.groupe);
      
    }
  });

  return User;
};