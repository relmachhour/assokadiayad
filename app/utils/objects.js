'use strick';




/***
* package dependencies
* @author fayssal tahtoub
* @email <fayssal.tahtoub@gmail.com>
*/
var _ = require('lodash');


module.exports.mongooseToJSONObject= function(value){
  return JSON.parse(JSON.stringify(value));
};

module.exports.deleteProperties= function(obj, stack){

  for(var i= 0;i < stack.length;i++)
    obj = _.omit(obj,stack[i]);

  return obj;
};
