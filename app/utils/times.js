'use strict';

/***
* package dependencies
* @author fayssal tahtoub <fayssal.tahtoub@gmail.com>
*/

var moment= require('moment'),
    _= require('lodash');


module.exports.incDateWith = function(linearDate){
  var tokens= linearDate.split(',');

	var momentFormatObj= {};

	_.each(tokens,function(token){
		var entries= token.trim().split(' ');
		momentFormatObj[entries[1]] = entries[0];
	});

	return moment().add(momentFormatObj);
};
