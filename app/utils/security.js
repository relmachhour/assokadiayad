'use strict';

/**
* this package give the essentials function to manage the security context
*/

/**
* Module dependencies
*/
var _ = require('lodash'),
    objects = require('./objects');
/**
* this function return
*/
module.exports.getToken= function(req){
  var token = req.headers["authorization"];
	if (typeof token !== 'undefined') {
    var items= token.split(' ');
      return items[1];
  }

	return undefined;
};

/**
* delete the gived cre
*/
module.exports.deleteCredentials= function(obj,stack){

  stack = ((typeof stack) !== 'undefined') ? stack : ['password', 'salt', 'provider', 'context', 'deleted', 'token', 'roles', 'expires'] ;


  var co = objects.deleteProperties(objects.mongooseToJSONObject(obj),stack);

  return co;

};
