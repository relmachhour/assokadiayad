'use strict';

/**
 * Module dependencies.
 */
var path = require('path'),
  errorHandler = require('./errors.server.controller'),
  _ = require('lodash'),
  db = require(path.resolve('./config/sequelize/sequelize')).models,
  Class = db.class;

/**
 * Create a classVar
 */
exports.create = function(req, res) {


  Class.create(req.body).then(function(classVar) {
    if (!classVar) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage("Probleme pendant l'entregistrement du Class dans la base de données")
      });
    } else {
      return res.jsonp(classVar);
    }
  }).catch(function(err) {
    return res.status(400).send({
      message: errorHandler.getErrorMessage(err)
    });
  });
};

/**
 * Show the current classVar
 */
exports.read = function(req, res) {
  res.json(req.classVar);
};

/**
 * Update a classVar
 */
exports.update = function(req, res) {
  var classVar = req.classVar;
  var updatedAttr = _.clone(req.body);

  updatedAttr = _.omit(updatedAttr,'id', 'user_id', 'user');

  classVar.updateAttributes(updatedAttr).then(function(classVar) { 
    
    res.json(classVar);
    
   }).catch(function(err) { 
    return res.status(400).send({ 
      message: errorHandler.getErrorMessage(err)
     });
   });
};

/**
 * Delete an classVar
 */
exports.delete = function(req, res) {
  var classVar = req.classVar;

  // Find the classVar
  Class.findById(classVar.id).then(function(classVar) {
    if (classVar) {
      // Delete the classVar
      classVar.destroy().then(function() {
        return res.json(classVar);
      }).catch(function(err) {
        return res.status(400).send({
          message: errorHandler.getErrorMessage(err)
        });
      });

    } else {
      return res.status(400).send({
        message: 'Unable to find the classVar'
      });
    }
  }).catch(function(err) {
    return res.status(400).send({
      message: errorHandler.getErrorMessage(err)
    });
  });

};

/**
 * List of Classes
 */
exports.list = function(req, res) {
  Class.findAll({
    include: [
      {model:db.student}
    ]
  }).then(function(classVares) {
    if (!classVares) {
      return res.status(404).send({
        message: 'No classVares found'
      });
    } else {
      res.json(classVares);
    }
  }).catch(function(err) {
    res.jsonp(err);
  });
};

/**
 * Class middleware
 */
exports.classVarByID = function(req, res, next, id) {

  if ((id % 1 === 0) === false) { //check if it's integer
    return res.status(404).send({
      message: 'Class is invalid'
    });
  }

  Class.find({
    where: {
      id: id
    },
    include: [
      
    ]
  }).then(function(classVar) {
    if (!classVar) {
      return res.status(404).send({
        message: 'No classVar with that identifier has been found'
      });
    } else {
      req.classVar = classVar;
      next();
    }
  }).catch(function(err) {
    return next(err);
  });

};