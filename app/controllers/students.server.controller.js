'use strict';

/**
 * Module dependencies.
 */
var path = require('path'),
  errorHandler = require('./errors.server.controller'),
  _ = require('lodash'),
  DB = require(path.resolve('./config/sequelize/sequelize')),
  db = require(path.resolve('./config/sequelize/sequelize')).models,
  Student = db.student,
  studentClass = db.student_class;

/**
 * Create a student
 */
exports.create = function(req, res) {

  console.log(req.body)
  var newStudent = req.body;
      newStudent.displayName = newStudent.firstName +' '+ newStudent.lastName;
  var listClasses = req.body.classes;
      delete newStudent.classes;

  DB.sequelize.transaction()
      .then( function ( t ) { 
          return Student.create( newStudent, { transaction: t } )
            .then( function ( stud ) {
              console.log('studId  ',stud.id)

              var promiseStdClass = listClasses.map(function(cls){
                    let newStdClass = {'studentId':stud.id,'classId':cls}; 
                    return studentClass.create(newStdClass, { transaction: t });
              });

              return Promise.all(promiseStdClass).then(function(clasStd){
                        console.log('clasStd', clasStd)
                        return t.commit();
              })

            } )
      .then( function (re1) {
          console.log('reStd  ',re1)
          res.json({returnedId:null})
      }, function (re2) {
          console.log('reStdClss  ',re2)
      } );

      }, function ( err ) {
          console.log( 'error', err );
          return res.status(400).send({
            message: errorHandler.getErrorMessage(err)
          });
  });


};

/**
 * Show the current student
 */
exports.read = function(req, res) {
  res.json(req.student);
};

/**
 * Update a student
 */
exports.update = function(req, res) {
  console.log(req.body)
  let student = req.body
  let oldClasses = req.body.classes;
  let newClasses = req.body.newClasses;
  delete student.classes;
  let listOldId = [];

  student.displayName = student.firstName +' '+ student.lastName;
  
  DB.sequelize.transaction()
      .then( function ( t ) { 
          return Student.update( student,{ where: { id: student.id} , transaction: t } )
            .then( function ( stud ) {

              let promiseOldClass = oldClasses.map(function(cls){
                    listOldId.push(cls.id)
                    let idx = newClasses.indexOf(cls.id);
                    if(idx==-1){
                      console.log('--Student to Remove----> cls.id:',cls.id,' student:',student.id)
                       return studentClass.destroy( { where: { student_id:student.id , class_id:cls.id },force:true, transaction: t } )
                    }                   
              });              
              return Promise.all(promiseOldClass).then(function(oldClasStd){
                  //  return t.commit();
                    
                    
                    let promiseNewClass = newClasses.map(function(clsId){
                          let idx = listOldId.indexOf(clsId);
                          if(idx==-1){
                             console.log('--Student to Create----> cls.id:',clsId,' student:',student.id)
                             return studentClass.create({'studentId':student.id,'classId':clsId}, { transaction: t });
                          }
                         
                    });
                    return Promise.all(promiseNewClass).then(function(newClasStd){
                        return t.commit();
                    })
              });

            } )
      .then( function (re1) {
          console.log('reStd  ',re1)
          res.json({returnedId:null})
      }, function (re2) {
          console.log('reStdClss O ',re2)
      }, function (re3) {
          console.log('reStdClss N ',re3)
      } );

      }, function ( err ) {
          console.log( 'error', err );
          return res.status(400).send({
            message: errorHandler.getErrorMessage(err)
          });
  });
















  /*var student = req.student;
  var updatedAttr = _.clone(req.body);

  updatedAttr = _.omit(updatedAttr,'id', 'user_id', 'user');

  student.updateAttributes(updatedAttr).then(function(student) { 
    
    res.json(student);
    
   }).catch(function(err) { 
    return res.status(400).send({ 
      message: errorHandler.getErrorMessage(err)
     });
   });*/
};

/**
 * Delete an student
 */
exports.delete = function(req, res) {
  var student = req.student;

  // Find the student
  Student.findById(student.id).then(function(student) {
    if (student) {
      // Delete the student
      student.destroy().then(function() {
        return res.json(student);
      }).catch(function(err) {
        return res.status(400).send({
          message: errorHandler.getErrorMessage(err)
        });
      });

    } else {
      return res.status(400).send({
        message: 'Unable to find the student'
      });
    }
  }).catch(function(err) {
    return res.status(400).send({
      message: errorHandler.getErrorMessage(err)
    });
  });

};

/**
 * List of Students
 */
exports.list = function(req, res) {
  Student.findAll({
    include: [
      {model: db.class}
    ]
  }).then(function(students) {
    if (!students) {
      return res.status(404).send({
        message: 'No students found'
      });
    } else {
      res.json(students);
    }
  }).catch(function(err) {
    res.jsonp(err);
  });
};

/**
 * Student middleware
 */
exports.studentByID = function(req, res, next, id) {

  if ((id % 1 === 0) === false) { //check if it's integer
    return res.status(404).send({
      message: 'Student is invalid'
    });
  }

  Student.find({
    where: {
      id: id
    },
    include: [
      {model: db.class}
    ]
  }).then(function(student) {
    if (!student) {
      return res.status(404).send({
        message: 'No student with that identifier has been found'
      });
    } else {
      req.student = student;
      next();
    }
  }).catch(function(err) {
    return next(err);
  });

};