'use strict';

/**
 * Module dependencies.
 */
var path = require('path'),
  errorHandler = require('./errors.server.controller'),
  _ = require('lodash'),
  DB = require(path.resolve('./config/sequelize/sequelize')),
  db = require(path.resolve('./config/sequelize/sequelize')).models,
  Income = db.income;

/**
 * Create a income
 */
exports.create = function(req, res) {


  Income.create(req.body).then(function(income) {
    if (!income) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage("Probleme pendant l'entregistrement du Income dans la base de données")
      });
    } else {
      return res.jsonp(income);
    }
  }).catch(function(err) {
    return res.status(400).send({
      message: errorHandler.getErrorMessage(err)
    });
  });
};

/**
 * Show the current income
 */
exports.read = function(req, res) {
  res.json(req.income);
};

/**
 * Update a income
 */
exports.update = function(req, res) {
  var income = req.income;
  var updatedAttr = _.clone(req.body);

  updatedAttr = _.omit(updatedAttr,'id', 'user_id', 'user');

  income.updateAttributes(updatedAttr).then(function(income) { 
    
    res.json(income);
    
   }).catch(function(err) { 
    return res.status(400).send({ 
      message: errorHandler.getErrorMessage(err)
     });
   });
};

/**
 * Delete an income
 */
exports.delete = function(req, res) {
  var income = req.income;

  // Find the income
  Income.findById(income.id).then(function(income) {
    if (income) {
      // Delete the income
      income.destroy().then(function() {
        return res.json(income);
      }).catch(function(err) {
        return res.status(400).send({
          message: errorHandler.getErrorMessage(err)
        });
      });

    } else {
      return res.status(400).send({
        message: 'Unable to find the income'
      });
    }
  }).catch(function(err) {
    return res.status(400).send({
      message: errorHandler.getErrorMessage(err)
    });
  });

};

/**
 * List of Incomes
 */
exports.list = function(req, res) {
  Income.findAll({
    include: [
      {model:db.typeIncome}
    ]
  }).then(function(incomes) {
    if (!incomes) {
      return res.status(404).send({
        message: 'No incomes found'
      });
    } else {
      res.json(incomes);
    }
  }).catch(function(err) {
    res.jsonp(err);
  });
};

/**
 * Income middleware
 */
exports.incomeByID = function(req, res, next, id) {

  if ((id % 1 === 0) === false) { //check if it's integer
    return res.status(404).send({
      message: 'Income is invalid'
    });
  }

  Income.find({
    where: {
      id: id
    },
    include: [
       {model:db.typeIncome},
       {model:db.student}
    ]
  }).then(function(income) {
    if (!income) {
      return res.status(404).send({
        message: 'No income with that identifier has been found'
      });
    } else {
      req.income = income;
      next();
    }
  }).catch(function(err) {
    return next(err);
  });

};