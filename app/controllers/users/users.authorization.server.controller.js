'use strict';

/**
 * Module dependencies.
 */
var _ = require('lodash'),
	security = require('../../utils/security'),
	path = require("path"),
	db = require(path.resolve('./config/sequelize/sequelize')).models,
	User = db.user;



/**
 * User middleware
 */
exports.userByID = function(req, res, next, id) {
	User.findById(id).then(function(user){
	  	if (!user) return next(new Error('Failed to load User ' + id));
			user.password = undefined
			user.salt = undefined
			req.profile = user;

		next();
	}).catch(function(err) {
		console.log('=================USER  authorizations =====>ERROR findById=========>')
		console.log(err)
	    return next(err);
	});

};

/**
 * Require login routing middleware
 */
exports.requiresLogin = function(req, res, next) {
	// check for login token here

	var token =	req.session.token
	var date = new Date()

console.log('TOKEN___> ',token)
	if(token !== undefined){
		// query DB for the user corresponding to the token and act accordingly
		
		User.findOne({ where: {token: token,expires: {$gte:date} }, raw: true }).then(function(user){
		  	if(!user){
		  		console.log('----------_> NO USER ')
				return res.status(401).send({
					message: 'Permissions denied, please login again.'
				});
			}
			// bind user object to request and continue

			req.token = user.token;
			user.password = undefined
			user.salt = undefined
			req.user = user;

			return next();


		}).catch(function(err) {
			console.log('=================USER  authorizations requiresLogin =====>ERROR findOne=========>')
			console.log(err)
		    return res.status(500).send({
				message: 'There was an internal server, login required'
			});
		});

	}else{
		console.log('vvvvvvvvvvvvvvvvvvvv')
		return res.status(401).send({
			message: 'Permissions denied, please login.'
		});
	}

};

/**
 * User authorizations routing middleware
 */
exports.hasAuthorization = function(roles) {
	var _this = this;
	return function(req, res, next) {
		_this.requiresLogin(req, res, function() {
			let token = req.session.token

			User.findOne({ where: {token: token}, raw: true }).then(function(user){

				if((roles.indexOf(req.user.roles) > -1)==true)
				{
					return next();
				}else{
					return res.status(403).send({
						message: 'Permissions denied, you dont have permissions.ZZZZZZZZ'
					});
				}


			}).catch(function(err) {
				console.log('=================USER  authorizations hasAuthorization =====>ERROR findOne=========>')
				console.log(err)
			    return res.status(403).send({
					message: 'Permissions denied, you dont have permissions.aa'
				});
			});


			});
	};

};
