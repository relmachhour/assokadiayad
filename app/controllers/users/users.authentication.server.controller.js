'use strict';

/**
 * Module dependencies.
 */
var _ = require('lodash'),
	errorHandler = require('../errors.server.controller'),
	passport = require('passport'),
	config = require('../../../config/config'),
	moment = require('moment'),
	securityContext = require('../../utils/security'),
	path = require("path"),
	db = require(path.resolve('./config/sequelize/sequelize')).models,
	User = db.user;
	var crypto = require('crypto');

	var jwt = require('jwt-simple');

/**
 * Signup
 */
exports.signup = function(req, res) {
	// For security measurement we remove the roles from the req.body object


	delete req.body.roles;

	// Init Variables
	var user = new User(req.body);
	var message = null;

	// Add missing user fields
	user.provider = 'local-token';
	user.displayname = user.firstName + ' ' + user.lastName;

	// Then save the user
	user.save(function(err) {
		if (err) {
			console.log(err);
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			// Remove sensitive data before login
			user.password = undefined;
			user.salt = undefined;

			req.login(user, function(err) {
				if (err) {
					console.log('error req.login')
					res.status(400).send(err);
				} else {
					console.log('success req.login')
					res.json(user);
				}
			});
		}
	});
};

/**
 * Signin after passport authentication
 */
exports.signin = function(req, res, next) {

	var username = req.body.username
	var password = req.body.password

			User.find({
			    where: {
			      	username: req.body.username
			    }, 	raw: true
			}).then(function(user) {
			    if (!user) {
			    	console.log('======>not find user')
			    	res.status(400).send({
						message: "Wrong username or password"
					});
			    }

		      	else if (!password || encryptPassword(password,user.password,user.salt)==false) {
		      		console.log('=========>not find authenticate')
					res.status(400).send({
						message: "Wrong username or password"
					});
				}
				else if(user.deletedAt != null){
					res.status(400).send({
						message: 'Your acompte is deleted'
					});
			    }
			    else if(user.locked == true){
			    	res.status(400).send({
						message: 'Votre acompte is disabled'
					});
			    }else {
					user.password = undefined
					user.salt = undefined
					
					var expireTime = config.getExpireTokenTime();

					// generate login token
					var tokenPayload = {
						username: user.username,
						expires: expireTime
					};

					var token = user.username//jwt.encode(tokenPayload, config.jwtTokenSecret, config.jwtHashMethod);

					// add token and exp date to user object
					user.token = token;
					user.expires = expireTime;
					User.update(user,{ where: { id: user.id } }).then(function(result) { 
					    req.token = token;
					    req.session.user = user;
					    req.session.token = req.token;
					    req.user = user;
					    res.json({token: user.token, roles: user.roles, context: user.context, name: user.displayName, local_token:user._id});
				    }).catch(function(err) {
				    	console.log('======================>error catch save passport')
					    res.json(err)
				    })

					
	
				}
			}).catch(function(err) {
				console.log('======================>ERROR FIRST LOCAL=========>')
				console.log(err)
				return err
			});
};




var makeSalt= function() {
    return crypto.randomBytes(16).toString('base64');
};
var authenticate = function(plainText,hashedPassword,salt) {
	console.log('****authenticate****')
    return encryptPassword(plainText, salt) === hashedPassword;

};
var encryptPassword = function(password, salt) {
    if (!password || !salt)
      return '';
    salt = new Buffer(salt, 'base64');
    return crypto.pbkdf2Sync(password, salt, 10000, 64,'sha512').toString('base64');
};


/**
 * Signout
 */
exports.signout = function(req, res) {
console.log('in signout')
//	var token = securityContext.getToken(req);

	var token = req.token//req.user.token;


	User.find({where: {token: token},attributes: ['id','token','expires','token'],raw:true}).then(function(user) {
		if(!user){
			console.log('signout not find user')
			res.status(400).send('no user');
			var socketio = req.app.get('socketio');
	  			socketio.sockets.emit('user.signout',token);
				res.redirect('/#!');
		}else{
			var yesterday = new Date();
			yesterday.setDate(yesterday.getDate() - 1);
			var ID = user.id
			delete user.id;
			user.token = '';
			user.expires = new Date(yesterday);
			req.user = undefined;
			req.token = undefined;
			User.update(user,{ where: { id: ID } }).then(function(result) { 
				console.log('success')
				var socketio = req.app.get('socketio');
	  			socketio.sockets.emit('user.signout',token);
				res.redirect('/#!');


		    }).catch(function(err) {
		    	console.log('======================>error signout save after remove token')
		    	console.log(err)
		       	res.status(400).send({message: ' error on logout'});
		    })
		}
	}).catch(function(err) {
		console.log('======================>signout ERROR find user=========>')
		console.log(err)
	    res.status(400).send(err);
	});
};

exports.userOut = function(req, res) {
	console.log('in outout')
		var token = securityContext.getToken(req);
	console.log(token)
	var socketio = req.app.get('socketio');
	socketio.sockets.emit('user.signout',token);
	//res.redirect('/#!');


	
};

/**
 * OAuth callback
 */
exports.oauthCallback = function(strategy) {
	return function(req, res, next) {
		passport.authenticate(strategy, function(err, user, redirectURL) {
			if (err || !user) {
				return res.redirect('/#!/accounts/signin');
			}
			req.login(user, function(err) {
				if (err) {
					return res.redirect('/#!/accounts/signin');
				}

				return res.redirect(redirectURL || '/');
			});
		})(req, res, next);
	};
};

/**
 * Helper function to save or update a OAuth user profile
 */
exports.saveOAuthUserProfile = function(req, providerUserProfile, done) {
	if (!req.user) {
		// Define a search query fields
		var searchMainProviderIdentifierField = 'providerData.' + providerUserProfile.providerIdentifierField;
		var searchAdditionalProviderIdentifierField = 'additionalProvidersData.' + providerUserProfile.provider + '.' + providerUserProfile.providerIdentifierField;

		// Define main provider search query
		var mainProviderSearchQuery = {};
		mainProviderSearchQuery.provider = providerUserProfile.provider;
		mainProviderSearchQuery[searchMainProviderIdentifierField] = providerUserProfile.providerData[providerUserProfile.providerIdentifierField];

		// Define additional provider search query
		var additionalProviderSearchQuery = {};
		additionalProviderSearchQuery[searchAdditionalProviderIdentifierField] = providerUserProfile.providerData[providerUserProfile.providerIdentifierField];

		// Define a search query to find existing user with current provider profile
		var searchQuery = {
			$or: [mainProviderSearchQuery, additionalProviderSearchQuery]
		};

		User.findOne(searchQuery, function(err, user) {
			if (err) {
				return done(err);
			} else {
				if (!user) {
					var possibleUsername = providerUserProfile.username || ((providerUserProfile.email) ? providerUserProfile.email.split('@')[0] : '');

					User.findUniqueUsername(possibleUsername, null, function(availableUsername) {
						user = new User({
							firstName: providerUserProfile.firstName,
							lastName: providerUserProfile.lastName,
							username: availableUsername,
							displayname: providerUserProfile.displayname,
							email: providerUserProfile.email,
							provider: providerUserProfile.provider,
							providerData: providerUserProfile.providerData
						});

						// And save the user
						user.save(function(err) {
							return done(err, user);
						});
					});
				} else {
					return done(err, user);
				}
			}
		});
	} else {
		// User is already logged in, join the provider data to the existing user
		var user = req.user;

		// Check if user exists, is not signed in using this provider, and doesn't have that provider data already configured
		if (user.provider !== providerUserProfile.provider && (!user.additionalProvidersData || !user.additionalProvidersData[providerUserProfile.provider])) {
			// Add the provider data to the additional provider data field
			if (!user.additionalProvidersData) user.additionalProvidersData = {};
			user.additionalProvidersData[providerUserProfile.provider] = providerUserProfile.providerData;

			// Then tell mongoose that we've updated the additionalProvidersData field
			user.markModified('additionalProvidersData');

			// And save the user
			user.save(function(err) {
				return done(err, user, '/#!/settings/accounts');
			});
		} else {
			return done(new Error('User is already connected using this provider'), user);
		}
	}
};

/**
 * Remove OAuth provider
 */
exports.removeOAuthProvider = function(req, res, next) {
	var user = req.user;
	var provider = req.param('provider');

	if (user && provider) {
		// Delete the additional provider
		if (user.additionalProvidersData[provider]) {
			delete user.additionalProvidersData[provider];

			// Then tell mongoose that we've updated the additionalProvidersData field
			user.markModified('additionalProvidersData');
		}

		user.save(function(err) {
			if (err) {
				return res.status(400).send({
					message: errorHandler.getErrorMessage(err)
				});
			} else {
				req.login(user, function(err) {
					if (err) {
						res.status(400).send(err);
					} else {
						res.json(user);
					}
				});
			}
		});
	}
};
