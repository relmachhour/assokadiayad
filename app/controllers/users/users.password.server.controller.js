'use strict';

/**
 * Module dependencies.
 */
var _ = require('lodash'),
	errorHandler = require('../errors.server.controller'),
	passport = require('passport'),
	config = require('../../../config/config'),
	nodemailer = require('nodemailer'),
	async = require('async'),
	crypto = require('crypto'),
	path = require("path"),
	moment  =require('moment'),
	db = require(path.resolve('./config/sequelize/sequelize')).models,
	User = db.user;

/**
 * Forgot for reset password (forgot POST)
 */
exports.forgot = function(req, res, next) {
	console.log('**************** FORGOT *******************')
	async.waterfall([
		// Generate random token
		function(done) {
			crypto.randomBytes(20, function(err, buffer) {
				var token = buffer.toString('hex');
				done(err, token);
			});
		},
		// Lookup user by username
		function(token, done) {
			if (req.body.email) {
				
				User.findOne({ where: {email: req.body.email}, raw: true }).then(function(user){
					// bind user object to request and continue

					if (user.provider !== 'local-token') {
						return res.status(400).send({
							message: 'It seems like you signed up using your ' + user.provider + ' account'
						});
					} else {
						user.resetPasswordToken = token;
						user.resetPasswordExpires = config.getMaxTimePasswordReset();
						User.update(user,{ where: { id: user.id } }).then(function(result) { 
					          console.log('SAVE SUCCESS============>***')
					          done(undefined, token, user);

					    }).catch(function(err) {
					          console.log('======================>error switchState')
					          console.log(err)
					           done(err, token, user);
					    })
					}
				}).catch(function(err) {
					console.log('=================USER  authorizations requiresLogin =====>ERROR user=========>')
					console.log(err)
				   	return res.status(400).send({
						message: 'No account with that email has been found'
					});
				});
			} else {
				return res.status(400).send({
					message: 'Email must not be blank'
				});
			}


		},
		function(token, user, done) {
			res.render('templates/reset-password-email', {
				name: user.firstname+' '+user.lastname,
				appName: config.app.title,
				url: 'http://' + req.headers.host + '/#!/accounts/password/reset/' + token
			}, function(err, emailHTML) {
				done(err, emailHTML, user);
			});
		},
		// If valid email, send reset email using service
		function(emailHTML, user, done) {
			var smtpTransport = nodemailer.createTransport(config.mailer.options);
			var mailOptions = {
				to: user.email,
				from: config.mailer.from,
				subject: 'Password Reset',
				html: emailHTML
			};
			smtpTransport.sendMail(mailOptions, function(err) {
				if (!err) {
					res.send({
						message: 'An email has been sent to ' + user.email + ' with further instructions.'
					});
				}

				done(err);
			});
		}
	], function(err) {
		if (err) return next(err);
	});
};

/**
 * Reset password GET from email token
 */
exports.validateResetToken = function(req, res) {
	User.findOne({
		resetPasswordToken: req.params.token,
		resetPasswordExpires: {
			$gt: Date.now()
		}
	}, function(err, user) {
		if (!user) {
			res.status(400).send({message: "Token not valid or has been expired"});
		}

		res.send({message: "the token is valid"});
	});
};

/**
 * Reset password POST from email token
 */
exports.reset = function(req, res, next) {
	// Init Variables
	var passwordDetails = req.body;
	var date = moment(new Date()).format('YYYY-MM-DD HH:mm:ss')
	console.log(date)
	console.log('*****A')
	async.waterfall([
		
		function(done) {
			
			User.findOne({ where: {resetPasswordToken: req.params.token}, raw: true }).then(function(user){
				console.log(user)
				
				if(passwordDetails.newPassword.length >= 4 && passwordDetails.newPassword.length <= 20){
					if (passwordDetails.newPassword === passwordDetails.verifyPassword) {
						//update password
						user.salt = undefined;
						user.password = passwordDetails.newPassword;
						user.resetPasswordToken = undefined;
						user.resetPasswordExpires = undefined;

						//disconnect user
						user.token = undefined;
						user.expires = undefined;

						user.salt = makeSalt();
			   			user.password = encryptPassword(user.password, user.salt);

						User.update(user,{ where: { id: user.id } }).then(function(result) { 
					          console.log('SAVE SUCCESS============>***')
					          done(undefined, user);

					    }).catch(function(err) {
					        console.log('======================>error switchState')
					        console.log(err)
					        return res.status(400).send({
									message: errorHandler.getErrorMessage(err)
							});
					    })
					} else {
						return res.status(400).send({
							message: 'Passwords do not match'
						});
					}
				}else{
					return res.status(400).send({
						message: 'Password must be between 6 and 20 characters.'
					});
				}

				
			}).catch(function(err) {
				console.log('=================USER  authorizations requiresLogin =====>ERROR user=========>')
				console.log(err)
			   	return res.status(400).send({
					message: 'Password reset token is invalid or has expired.'
				});
			});

























			/*
			User.findOne({
				resetPasswordToken: req.params.token,
				resetPasswordExpires: {
					$gt: Date.now()
				}
			}, function(err, user) {
				console.log('*****B')
				if (!err && user) {
					if(passwordDetails.newPassword.length >= 6 && passwordDetails.newPassword.length <= 20){
						if (passwordDetails.newPassword === passwordDetails.verifyPassword) {
							//update password
							user.salt = undefined;
							user.password = passwordDetails.newPassword;
							user.resetPasswordToken = undefined;
							user.resetPasswordExpires = undefined;

							//disconnect user
							user.token = undefined;
							user.expires = undefined;

							user.save(function(err,saved) {
								if (err) {
									return res.status(400).send({
										message: errorHandler.getErrorMessage(err)
									});
								} else {
									
									done(err, user);
								}
							});
						} else {
							return res.status(400).send({
								message: 'Passwords do not match'
							});
						}
					}else{
						return res.status(400).send({
							message: 'Password must be between 6 and 20 characters.'
						});
					}

				} else {
					return res.status(400).send({
						message: 'Password reset token is invalid or has expired.'
					});
				}
			});*/
		},
		function(user, done) {
			res.render('templates/reset-password-confirm-email', {
				name: user.displayname,
				appName: config.app.title
			}, function(err, emailHTML) {
				done(err, emailHTML, user);
			});
		},
		// If valid email, send reset email using service
		function(emailHTML, user, done) {
			var smtpTransport = nodemailer.createTransport(config.mailer.options);
			var mailOptions = {
				to: user.email,
				from: config.mailer.from,
				subject: 'Your password has been changed',
				html: emailHTML
			};

			smtpTransport.sendMail(mailOptions, function(err) {
				done(err, 'done');
			});
		}
	], function(err) {
		if (err) return next(err);
		else return res.send({
			message: 'Password has been modified successfully'
		});
	});
};

















function makeSalt() {
    return crypto.randomBytes(16).toString('base64');
};

function encryptPassword(password, salt) {
	if (!password || !salt)
	  return '';
	salt = new Buffer(salt, 'base64');
	return crypto.pbkdf2Sync(password, salt, 10000, 64).toString('base64');
};