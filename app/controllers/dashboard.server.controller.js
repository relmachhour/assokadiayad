'use strict';

/**
 * Module dependencies.
 */
var path = require('path'),
  errorHandler = require('./errors.server.controller'),
  _ = require('lodash'),
  async = require('async'),
  moment = require('moment'),
  pg = require(path.resolve('./config/sequelize/pg')).pg,
  DB = require(path.resolve('./config/sequelize/sequelize')),
  db = require(path.resolve('./config/sequelize/sequelize')).models;



exports.dashReport = function(req, res) {
    async.parallel([
      function(callback) { 
        //top dash
        let query = "select ( select case when(select sum(amount) from incomes where date(saved_at) = current_date) is null then 0 else (select sum(amount) from incomes where date(saved_at) = current_date) end as income_day) , ( select case when (select sum(amount) from expenses where date(saved_at) = current_date) is null then 0 else (select sum(amount) from expenses where date(saved_at) = current_date) end as expense_day ) , ( select case when sum(amount) is null then 0 else sum(amount) end as income_month from incomes where extract('year' from saved_at) = extract('year' from CURRENT_DATE) and extract('month' from saved_at) = extract('month' from CURRENT_DATE) ) ,( select case when sum(amount) is null then 0 else sum(amount) end as expense_month from expenses where extract('year' from saved_at) = extract('year' from CURRENT_DATE) and extract('month' from saved_at) = extract('month' from CURRENT_DATE) )"
        
        pg.query(query, (err, dbRes) => {
          if(err){
            console.log(err)
            callback(null,[])
          }else{
            callback(null,dbRes.rows)
          }
        })

        
      },
      function(callback) { 
        // pie expenses
        let query = "select array_agg(amount) as amount, array_agg(name) as name from ( select sum(amount) as amount ,type_expenses.name from expenses,type_expenses where expenses.type_expense_id = type_expenses.id and extract('year' from saved_at) = extract('year' from CURRENT_DATE) and extract('month' from saved_at) = extract('month' from CURRENT_DATE) group by type_expenses.id order by sum(amount) desc )z"
        pg.query(query, (err, dbRes) => {
          if(err){
            console.log(err)
            callback(null,[])
          }else{
            callback(null,dbRes.rows)
          }
        })
      },
      function(callback) { 
        // pie incomes
        let query = "select array_agg(amount) as amount, array_agg(name) as name from (select sum(amount) as amount ,type_incomes.name from incomes,type_incomes where incomes.type_income_id = type_incomes.id and extract('year' from saved_at) = extract('year' from CURRENT_DATE) and extract('month' from saved_at) = extract('month' from CURRENT_DATE) group by type_incomes.id order by sum(amount) desc) z"
        pg.query(query, (err, dbRes) => {
          if(err){
            console.log(err)
            callback(null,[])
          }else{
            callback(null,dbRes.rows)
          }
        })
      },
      function(callback) { 
        //graph expenses and incomes month by days
        //let query = "select array_agg(days) as days,array_agg(incomes) as incomes,array_agg(expenses) as expenses from ( SELECT days.days, case when a.amount is null then 0 else a.amount end as incomes, case when b.amount is null then 0 else b.amount end as expenses FROM generate_series(1, 31) days left join( select sum(amount) as amount ,extract('days' from saved_at) as days from incomes where extract('year' from saved_at) = extract('year' from CURRENT_DATE) and extract('month' from saved_at) = extract('month' from CURRENT_DATE) group by extract('days' from saved_at) order by extract('days' from saved_at) asc )a on a.days = days.days left join( select sum(amount) as amount ,extract('days' from saved_at) as days from expenses where extract('year' from saved_at) = extract('year' from CURRENT_DATE) and extract('month' from saved_at) = extract('month' from CURRENT_DATE) group by extract('days' from saved_at) order by extract('days' from saved_at) asc )b on b.days = days.days ) b where b.days <= extract('days' from current_date)"
        let query = "select array_agg(months) as months,array_agg(incomes) as incomes,array_agg(expenses) as expenses from ( SELECT months.months, case when a.amount is null then 0 else a.amount end as incomes, case when b.amount is null then 0 else b.amount end as expenses FROM generate_series(1, 12) months left join( select sum(amount) as amount ,extract('month' from saved_at) as months from incomes where extract('year' from saved_at) = extract('year' from CURRENT_DATE) and extract('month' from saved_at) = extract('month' from CURRENT_DATE) group by extract('month' from saved_at) order by extract('month' from saved_at) asc )a on a.months = months.months left join( select sum(amount) as amount ,extract('month' from saved_at) as months from expenses where extract('year' from saved_at) = extract('year' from CURRENT_DATE) and extract('month' from saved_at) = extract('month' from CURRENT_DATE) group by extract('month' from saved_at) order by extract('month' from saved_at) asc )b on b.months = months.months ) b where b.months <= extract('month' from current_date)"

        //SELECT months.months, a.amount as incomes, b.amount  as expenses FROM generate_series(1, 12) months left join( select sum(amount) as amount ,extract('month' from saved_at) as months from incomes where extract('year' from saved_at) = extract('year' from CURRENT_DATE) and extract('month' from saved_at) = extract('month' from CURRENT_DATE) group by extract('month' from saved_at) order by extract('month' from saved_at) asc )a on a.months = months.months left join( select sum(amount) as amount ,extract('month' from saved_at) as months from expenses where extract('year' from saved_at) = extract('year' from CURRENT_DATE) and extract('month' from saved_at) = extract('month' from CURRENT_DATE) group by extract('month' from saved_at) order by extract('month' from saved_at) asc )b on b.months = months.months
        pg.query(query, (err, dbRes) => {
          if(err){
            console.log(err)
            callback(null,[])
          }else{
            console.log("dbRes.rows")
            console.log(dbRes.rows)
            callback(null,dbRes.rows)
          }
        })
      },
      function(callback) { 
        //graph pie student count payment
        let query = "select (select count(id) from students where deleted_at is null) as total_student , case when count(*) is null then 0 else count(*) end as student_payement from type_incomes,incomes where type_incomes.id = incomes.type_income_id and type_incomes.name = 'الانخراطات' and EXTRACT('year' from incomes.saved_at) = EXTRACT('year' from current_date) and EXTRACT('month' from incomes.participation_date) = EXTRACT('month' from current_date) and type_incomes.deleted_at is null and incomes.deleted_at is null"
        pg.query(query, (err, dbRes) => {
          if(err){
            console.log(err)
            callback(null,[])
          }else{
            callback(null,dbRes.rows)
          }
        })
      },
      function(callback) { 
        //list sduent paym
        let query = "select students.id, students.first_name ||' '|| students.last_name as name from type_incomes,incomes,students where type_incomes.id = incomes.type_income_id and incomes.student_id = students.id and type_incomes.name = 'الانخراطات' and EXTRACT('year' from incomes.saved_at) = EXTRACT('year' from current_date) and EXTRACT('month' from incomes.saved_at) = EXTRACT('month' from current_date) and type_incomes.deleted_at is null and incomes.deleted_at is null"
        pg.query(query, (err, dbRes) => {
          if(err){
            console.log(err)
            callback(null,[])
          }else{
            callback(null,dbRes.rows)
          }
        })
      }
  ], function(err, results) {
      
      let backData = {'kpi':results[0],'pieE':results[1],'pieI':results[2],'graphs':results[3],'pieP':results[4],'listStudent':results[5]}
      res.json(backData)
  });
      
};



exports.find = function(req,res)
{
  let filter  = req.body;
  let from    = moment(filter.from).format('YYYY-MM-DD'); 
  let to      = moment(filter.to).format('YYYY-MM-DD'); 

  let query = "select 'مصروف' as type, date(expenses.saved_at), expenses.operation,expenses.project,expenses.document,expenses.amount, type_expenses.name as type_name, case when expenses.type_paiement = 'bank' then 'البنك' else 'الصندوق' end as type_paiement from expenses left join type_expenses on expenses.type_expense_id = type_expenses.id where date(expenses.saved_at)>= date('"+from+"') and date(expenses.saved_at)<= date('"+to+"') union all select 'مدخول' as type, date(incomes.saved_at), incomes.operation,incomes.project,incomes.document,incomes.amount, type_incomes.name as type_name, case when incomes.type_paiement = 'bank' then 'البنك' else 'الصندوق' end as type_paiement from incomes left join type_incomes on incomes.type_income_id = type_incomes.id where date(incomes.saved_at)>= date('"+from+"') and date(incomes.saved_at)<= date('"+to+"') order by date desc"
  console.log(query)
  pg.query(query, (err, dbRes) => {
    if(err){
      console.log(err)
      res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    }else{
      res.json(dbRes.rows)
    }
  })
};


exports.down = function(req,res)
{
  let filter    = req.body;
  let from      = moment(filter.from).format('YYYY-MM-DD'); 
  let to        = moment(filter.to).format('YYYY-MM-DD'); 
  
  let folderName = path.resolve('/data')
  let fileName =  'export_'+new Date().valueOf()+'.csv'
  let urlPath = path.join(folderName, fileName)

    console.log('----------_>',folderName+fileName+'----->'+urlPath)
  let query = "select date(expenses.saved_at) as \"الناريخ\", 'مصروف' as \"طبيعة العملية\" , type_expenses.name as \"نوع العملية\", expenses.operation as \"العملية\", expenses.project as \"المشروع\", expenses.document as \"الوثيقة\", case when expenses.type_paiement = 'bank' then 'البنك' else 'الصندوق' end as \"طريقة التسديد\", expenses.amount as \"المبلغ\" from expenses left join type_expenses on expenses.type_expense_id = type_expenses.id where date(expenses.saved_at)>= date('"+from+"') and date(expenses.saved_at)<= date('"+to+"') union all select date(incomes.saved_at) as \"الناريخ\", 'مدخول' as \"طبيعة العملية\" , type_incomes.name as \"نوع العملية\", incomes.operation as \"العملية\", incomes.project as \"المشروع\", incomes.document as \"الوثيقة\", case when incomes.type_paiement = 'bank' then 'البنك' else 'الصندوق' end as \"طريقة التسديد\", incomes.amount as \"المبلغ\" from incomes left join type_incomes on incomes.type_income_id = type_incomes.id where date(incomes.saved_at)>= date('"+from+"') and date(incomes.saved_at)<= date('"+to+"') order by \"الناريخ\" desc"
  let copy = "COPY ("+query+") TO '"+urlPath+"' DELIMITER ',' CSV HEADER;"
  console.log(copy)
  pg.query(copy, (err, dbRes) => {
    if(err){
      console.log(err)
      res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    }else{
      res.json({url:fileName})
    }
  })
};




exports.fundTable = function(req,res)
{
  let filter    = req.body;
  let from      = moment(filter.from).format('YYYY-MM-DD'); 
  let to        = moment(filter.to).format('YYYY-MM-DD'); 

  let query = "select z.days,z.operation, sum(z.incomes) as incomes, sum(z.expenses) expenses from ( select days::date,a.operation, case when a.type='expenses' then a.amount else 0 end as expenses, case when a.type='incomes' then a.amount else 0 end as incomes from generate_series( '"+from+"'::date, '"+to+"'::date, '1 day'::interval ) days left join ( select saved_at,amount,operation, 'expenses' as type from expenses where type_paiement = 'fund' union select saved_at,amount,operation, 'incomes' as type from incomes where type_paiement = 'fund' order by saved_at desc )a on date(a.saved_at) = date(days) )z where z.operation is not null group by z.days,z.operation order by z.days desc"
console.log(query)

  DB.sequelize.query(query, { type: DB.sequelize.QueryTypes.SELECT}).then(result => {
      res.json(result)
  }).catch(function(err) {
    console.log(err)
    res.status(400).send({
        message: errorHandler.getErrorMessage(err)
    });
  })
}

exports.bankTable = function(req,res)
{
  let filter    = req.body;
  let from      = moment(filter.from).format('YYYY-MM-DD'); 
  let to        = moment(filter.to).format('YYYY-MM-DD'); 

  let query = "select z.days,z.operation, sum(z.incomes) as incomes, sum(z.expenses) expenses from ( select days::date,a.operation, case when a.type='expenses' then a.amount else 0 end as expenses, case when a.type='incomes' then a.amount else 0 end as incomes from generate_series( '"+from+"'::date, '"+to+"'::date, '1 day'::interval ) days left join ( select saved_at,amount,operation, 'expenses' as type from expenses where type_paiement = 'bank' union select saved_at,amount,operation, 'incomes' as type from incomes where type_paiement = 'bank' order by saved_at desc )a on date(a.saved_at) = date(days) )z where z.operation is not null group by z.days,z.operation order by z.days desc"
console.log(query)
  DB.sequelize.query(query, { type: DB.sequelize.QueryTypes.SELECT}).then(result => {
      res.json(result)
  }).catch(function(err) {
    console.log(err)
    res.status(400).send({
        message: errorHandler.getErrorMessage(err)
    });
  })


}

exports.incomeTable = function(req,res)
{
  let filter    = req.body;
  let from      = moment(filter.from).format('YYYY-MM-DD'); 
  let to        = moment(filter.to).format('YYYY-MM-DD'); 

  let query = "select type_incomes.name as type,incomes.saved_at, incomes.operation,incomes.document, case when incomes.amount is null then 0 else incomes.amount end as amount from type_incomes left join incomes on incomes.type_income_id = type_incomes.id where date(incomes.saved_at) >= date('"+from+"') and date(incomes.saved_at) <= date('"+to+"') order by incomes.saved_at desc, type_incomes.id"
console.log(query)

  DB.sequelize.query(query, { type: DB.sequelize.QueryTypes.SELECT}).then(result => {
      res.json(result)
  }).catch(function(err) {
    console.log(err)
    res.status(400).send({
        message: errorHandler.getErrorMessage(err)
    });
  })


}

exports.expenseTable = function(req,res)
{
  let filter    = req.body;
  let from      = moment(filter.from).format('YYYY-MM-DD'); 
  let to        = moment(filter.to).format('YYYY-MM-DD'); 

  let query = "select type_expenses.name as type,expenses.saved_at, expenses.operation,expenses.document, case when expenses.amount is null then 0 else expenses.amount end as amount from type_expenses left join expenses on expenses.type_expense_id = type_expenses.id where date(expenses.saved_at) >= date('"+from+"') and date(expenses.saved_at) <= date('"+to+"') order by expenses.saved_at desc, type_expenses.id"
console.log(query)

  DB.sequelize.query(query, { type: DB.sequelize.QueryTypes.SELECT}).then(result => {
      res.json(result)
  }).catch(function(err) {
    console.log(err)
    res.status(400).send({
        message: errorHandler.getErrorMessage(err)
    });
  })


}

exports.studentParticipation = function(req,res)
{
  let filter    = req.body;
  let from      = moment(filter.from).format('YYYY-MM-DD'); 
  let to        = moment(filter.to).format('YYYY-MM-DD'); 
  

  let query = "select incomes.saved_at,incomes.amount,incomes.participation_date, EXTRACT(YEAR FROM participation_date) as year,EXTRACT(MONTH FROM participation_date) as month,students.id,students.first_name || ' ' || students.last_name as name from incomes, type_incomes,students where type_incomes.id = incomes.type_income_id and incomes.student_id = students.id and incomes.deleted_at is null and type_incomes.deleted_at is null and type_incomes.name = 'الانخراطات' and date(incomes.participation_date) >= date('"+from+"') and date(incomes.participation_date) <= date('"+to+"')"
  if(req.body.student_id) query = query + 'and students.id = '+req.body.student_id;

console.log(query)

  DB.sequelize.query(query, { type: DB.sequelize.QueryTypes.SELECT}).then(result => {
      res.json(result)
  }).catch(function(err) {
    console.log(err)
    res.status(400).send({
        message: errorHandler.getErrorMessage(err)
    });
  })


}