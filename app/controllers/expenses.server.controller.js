'use strict';

/**
 * Module dependencies.
 */
var path = require('path'),
  errorHandler = require('./errors.server.controller'),
  _ = require('lodash'),
  DB = require(path.resolve('./config/sequelize/sequelize')),
  db = require(path.resolve('./config/sequelize/sequelize')).models,
  Expense = db.expense;

/**
 * Create a expense
 */
exports.create = function(req, res) {


  Expense.create(req.body).then(function(expense) {
    if (!expense) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage("Probleme pendant l'entregistrement du Expense dans la base de données")
      });
    } else {
      return res.jsonp(expense);
    }
  }).catch(function(err) {
    return res.status(400).send({
      message: errorHandler.getErrorMessage(err)
    });
  });
};

/**
 * Show the current expense
 */
exports.read = function(req, res) {
  res.json(req.expense);
};

/**
 * Update a expense
 */
exports.update = function(req, res) {
  var expense = req.expense;
  var updatedAttr = _.clone(req.body);

  updatedAttr = _.omit(updatedAttr,'id', 'user_id', 'user');

  expense.updateAttributes(updatedAttr).then(function(expense) { 
    
    res.json(expense);
    
   }).catch(function(err) { 
    return res.status(400).send({ 
      message: errorHandler.getErrorMessage(err)
     });
   });
};

/**
 * Delete an expense
 */
exports.delete = function(req, res) {
  var expense = req.expense;

  // Find the expense
  Expense.findById(expense.id).then(function(expense) {
    if (expense) {
      // Delete the expense
      expense.destroy().then(function() {
        return res.json(expense);
      }).catch(function(err) {
        return res.status(400).send({
          message: errorHandler.getErrorMessage(err)
        });
      });

    } else {
      return res.status(400).send({
        message: 'Unable to find the expense'
      });
    }
  }).catch(function(err) {
    return res.status(400).send({
      message: errorHandler.getErrorMessage(err)
    });
  });

};

/**
 * List of Expenses
 */
exports.list = function(req, res) {
  Expense.findAll({
    include: [
      {model:db.typeExpense}
    ]
  }).then(function(expenses) {
    if (!expenses) {
      return res.status(404).send({
        message: 'No expenses found'
      });
    } else {
      res.json(expenses);
    }
  }).catch(function(err) {
    res.jsonp(err);
  });
};

/**
 * Expense middleware
 */
exports.expenseByID = function(req, res, next, id) {

  if ((id % 1 === 0) === false) { //check if it's integer
    return res.status(404).send({
      message: 'Expense is invalid'
    });
  }

  Expense.find({
    where: {
      id: id
    },
    include: [
       {model:db.typeExpense}
    ]
  }).then(function(expense) {
    if (!expense) {
      return res.status(404).send({
        message: 'No expense with that identifier has been found'
      });
    } else {
      req.expense = expense;
      next();
    }
  }).catch(function(err) {
    return next(err);
  });

};