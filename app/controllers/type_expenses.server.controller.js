'use strict';

/**
 * Module dependencies.
 */
var path = require('path'),
  errorHandler = require('./errors.server.controller'),
  _ = require('lodash'),
  DB = require(path.resolve('./config/sequelize/sequelize')),
  db = require(path.resolve('./config/sequelize/sequelize')).models,
  Type_expense = db.typeExpense;

/**
 * Create a type_expense
 */
exports.create = function(req, res) {


  Type_expense.create(req.body).then(function(type_expense) {
    if (!type_expense) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage("Probleme pendant l'entregistrement du Type_expense dans la base de données")
      });
    } else {
      return res.jsonp(type_expense);
    }
  }).catch(function(err) {
    return res.status(400).send({
      message: errorHandler.getErrorMessage(err)
    });
  });
};

/**
 * Show the current type_expense
 */
exports.read = function(req, res) {
  res.json(req.type_expense);
};

/**
 * Update a type_expense
 */
exports.update = function(req, res) {
  var type_expense = req.type_expense;
  var updatedAttr = _.clone(req.body);

  updatedAttr = _.omit(updatedAttr,'id', 'user_id', 'user');

  type_expense.updateAttributes(updatedAttr).then(function(type_expense) { 
    
    res.json(type_expense);
    
   }).catch(function(err) { 
    return res.status(400).send({ 
      message: errorHandler.getErrorMessage(err)
     });
   });
};

/**
 * Delete an type_expense
 */
exports.delete = function(req, res) {
  var type_expense = req.type_expense;

  // Find the type_expense
  Type_expense.findById(type_expense.id).then(function(type_expense) {
    if (type_expense) {
      // Delete the type_expense
      type_expense.destroy().then(function() {
        return res.json(type_expense);
      }).catch(function(err) {
        return res.status(400).send({
          message: errorHandler.getErrorMessage(err)
        });
      });

    } else {
      return res.status(400).send({
        message: 'Unable to find the type_expense'
      });
    }
  }).catch(function(err) {
    return res.status(400).send({
      message: errorHandler.getErrorMessage(err)
    });
  });

};

/**
 * List of Type_expenses
 */
exports.list = function(req, res) {
  Type_expense.findAll({
    include: [
    ]
  }).then(function(type_expenses) {
    if (!type_expenses) {
      return res.status(404).send({
        message: 'No type_expenses found'
      });
    } else {
      res.json(type_expenses);
    }
  }).catch(function(err) {
    res.jsonp(err);
  });
};

/**
 * Type_expense middleware
 */
exports.type_expenseByID = function(req, res, next, id) {

  if ((id % 1 === 0) === false) { //check if it's integer
    return res.status(404).send({
      message: 'Type_expense is invalid'
    });
  }

  Type_expense.find({
    where: {
      id: id
    },
    include: [
    ]
  }).then(function(type_expense) {
    if (!type_expense) {
      return res.status(404).send({
        message: 'No type_expense with that identifier has been found'
      });
    } else {
      req.type_expense = type_expense;
      next();
    }
  }).catch(function(err) {
    return next(err);
  });

};