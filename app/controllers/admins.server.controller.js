'use strict';


/***
* controller package dependencies
*/

var errorHandler = require('./errors.server.controller'),
    security = require('../../app/utils/security'),
    _ = require('lodash'),
    crypto = require('crypto'),
    path = require("path"),
    db = require(path.resolve('./config/sequelize/sequelize')).models,
    User = db.user;



// read action
module.exports.read= function(req,res){
  res.jsonp(security.deleteCredentials(req.admin,['password', 'salt', 'provider', 'context', 'deleted', 'token', 'expires']));
};




module.exports.list=  function(req,res){
  var role = req.user.roles
  console.log('ROLE ',role)
  if(role=='ROLE_SUPER_ADMIN'){
    var rolesAutorized = ['ROLE_ADMIN','ROLE_USER']
  }else if(role=='ROLE_ADMIN'){
    var rolesAutorized = ['ROLE_ADMIN','ROLE_USER']
  }

  if(role=='ROLE_ADMIN' || role=='ROLE_SUPER_ADMIN'){
 
    User.findAll({ 
        where: { roles: { $in: rolesAutorized }},
        attributes: ['id','firstname','lastname','displayName','email','locked','username','roles']
     }).then(function(users) { 
      if (!users) { 
        return res.status(404).send({ 
          message: 'No users found'
         });
       } else { 
        return res.json(users);
       }
     })
    .catch(function(err) { 
      console.log(err)
      res.status(404).send({ message:'Error fetching data' });
     });
  }else{
    res.status(404).send({ message:'Not Autorized' });
  }

};


// create action
module.exports.create=  function(req,res){
   req.body.displayName= req.body.firstName + ' ' + req.body.lastName;

  let admin = User.build(req.body)
      admin.salt = makeSalt();
      admin.password = encryptPassword(admin.password, admin.salt);

      console.log('========================>admin')
      console.log(admin)
      admin.save().then(function(result) {
         console.log('success create user')
         console.log(result)
         var nzDoc= security.deleteCredentials(admin);
          res.jsonp(nzDoc);
      }).catch(function(err) {
          console.log(' catch error create user')
          console.log(err)
          res.status(400).send({'messages': errorHandler.getErrorMessage(err)});
      })

};


// update action
module.exports.update=  function(req,res){
  var user = req.body
  user.displayName = user.firstName+' '+user.lastName
  console.log(req.body)

    User.update(user,{ where: { id: user.id } }).then(function(result) { 
    console.log('SAVE SUCCESS============>***')
        var nzDoc= security.deleteCredentials(user);
        res.jsonp(nzDoc);
    }).catch(function(err) {
      console.log('======================>error update user')
      res.status(400).send({'messages': errorHandler.getErrorMessage(err)});
    })
};


// delete one entity action
module.exports.delete=  function(req,res){
    var admin = req.admin;
    User.destroy({ where: { id:admin.id},force:true}).then(function(user) {           
        res.json(user)      
     }).catch(function(err) { 
        res.status(400).send({ 
            message: errorHandler.getErrorMessage(err)
         });
     });
};

/**
* Admin switch state
*
*/
exports.switchState= function(req,res){
  var admin= req.admin;

    User.update({locked:!admin.locked},{ where: { id: admin.id } }).then(function(result) { 
        console.log('SAVE SUCCESS============>***')
        console.log(result)
        console.log('success')
        res.jsonp({status: true, state:result.locked})

    }).catch(function(err) {
        console.log('======================>error switchState')
        console.log(err)
         res.status(400).send({status:false});
    })
};

/**
*  get Role from token
*
*/
exports.getRole= function(req,res){
  console.log(req.token)
  console.log(req.user)
  User.findOne({ where: {token: req.token},raw:true }).then(function(user){
       if(!user) return (new Error('can\'t load the admin data'));

      res.jsonp({role: user.roles,groupe:user.groupe})
     
    }).catch(function(err) {
        console.log('======================>error getRole')
         return next(err);
    });

};



/**
* middleware
*/
module.exports.adminById= function(req, res, next, adminId){

  if(adminId != 'remove-all'){

    User.findOne({ where: {id: adminId},raw:true }).then(function(admin){
       if(!admin) return next(new Error('can\'t load the admin data'));

      req.admin =  admin;
      next();
    }).catch(function(err) {
        console.log('======================>error adminById')
         return next(err);
    });
  }else{
    next();
  }

};












exports.getProfil = function(req,res){

    if(req.user.id)
    {
      User.findOne({ 
          where: { id: req.user.id},
          attributes: ['id','displayname', 'email', 'firstname', 'lastname', 'profileImageURL', 'roles', 'telephone','adresse'],
          raw:true

      }).then(function(profil) { 
            if (!profil) { 
              return res.status(404).send({ 
                message: 'No profil data found'
               });
            } else { 
              return res.json(profil);
            }
      }).catch(function(err) { 
        console.log(err)
        res.status(404).send({ message:'Error profil query' });
       });

    }

}


exports.updateProfil = function(req,res){
  console.log('in update')
    var data = req.body
    console.log(req.body)
    if(req.user.id)
    {
      User.update(data,{ where: { id: req.user.id } }).then(function(result) { 
          console.log('SAVE SUCCESS============>***')
          console.log(result)
          console.log('success')
          res.jsonp(data)

      }).catch(function(err) {
          console.log('======================>error switchState')
          console.log(err)
           res.status(400).send({status:false});
      })

    }

}
















  var makeSalt= function() {
        return crypto.randomBytes(16).toString('base64');
     };
     var authenticate= function(plainText) {
        return this.encryptPassword(plainText, this.salt) === this.hashedPassword;
     };
    var encryptPassword = function(password, salt) {
        if (!password || !salt)
          return '';
        salt = new Buffer(salt, 'base64');
        return crypto.pbkdf2Sync(password, salt, 10000, 64,'sha512').toString('base64');
    }



