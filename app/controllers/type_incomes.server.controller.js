'use strict';

/**
 * Module dependencies.
 */
var path = require('path'),
  errorHandler = require('./errors.server.controller'),
  _ = require('lodash'),
  DB = require(path.resolve('./config/sequelize/sequelize')),
  db = require(path.resolve('./config/sequelize/sequelize')).models,
  Type_income = db.typeIncome;

/**
 * Create a type_income
 */
exports.create = function(req, res) {


  Type_income.create(req.body).then(function(type_income) {
    if (!type_income) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage("Probleme pendant l'entregistrement du Type_income dans la base de données")
      });
    } else {
      return res.jsonp(type_income);
    }
  }).catch(function(err) {
    return res.status(400).send({
      message: errorHandler.getErrorMessage(err)
    });
  });
};

/**
 * Show the current type_income
 */
exports.read = function(req, res) {
  res.json(req.type_income);
};

/**
 * Update a type_income
 */
exports.update = function(req, res) {
  var type_income = req.type_income;
  var updatedAttr = _.clone(req.body);

  updatedAttr = _.omit(updatedAttr,'id', 'user_id', 'user');

  type_income.updateAttributes(updatedAttr).then(function(type_income) { 
    
    res.json(type_income);
    
   }).catch(function(err) { 
    return res.status(400).send({ 
      message: errorHandler.getErrorMessage(err)
     });
   });
};

/**
 * Delete an type_income
 */
exports.delete = function(req, res) {
  var type_income = req.type_income;

  // Find the type_income
  Type_income.findById(type_income.id).then(function(type_income) {
    if (type_income) {
      // Delete the type_income
      type_income.destroy().then(function() {
        return res.json(type_income);
      }).catch(function(err) {
        return res.status(400).send({
          message: errorHandler.getErrorMessage(err)
        });
      });

    } else {
      return res.status(400).send({
        message: 'Unable to find the type_income'
      });
    }
  }).catch(function(err) {
    return res.status(400).send({
      message: errorHandler.getErrorMessage(err)
    });
  });

};

/**
 * List of Type_incomes
 */
exports.list = function(req, res) {
  Type_income.findAll({
    include: [
    ]
  }).then(function(type_incomes) {
    if (!type_incomes) {
      return res.status(404).send({
        message: 'No type_incomes found'
      });
    } else {
      res.json(type_incomes);
    }
  }).catch(function(err) {
    res.jsonp(err);
  });
};

/**
 * Type_income middleware
 */
exports.type_incomeByID = function(req, res, next, id) {

  if ((id % 1 === 0) === false) { //check if it's integer
    return res.status(404).send({
      message: 'Type_income is invalid'
    });
  }

  Type_income.find({
    where: {
      id: id
    },
    include: [
    ]
  }).then(function(type_income) {
    if (!type_income) {
      return res.status(404).send({
        message: 'No type_income with that identifier has been found'
      });
    } else {
      req.type_income = type_income;
      next();
    }
  }).catch(function(err) {
    return next(err);
  });

};