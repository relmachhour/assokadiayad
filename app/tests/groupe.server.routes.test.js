'use strict';

var should = require('should'),
	request = require('supertest'),
	app = require('../../server'),
	mongoose = require('mongoose'),
	User = mongoose.model('User'),
	Groupe = mongoose.model('Groupe'),
	agent = request.agent(app);

/**
 * Globals
 */
var credentials, user, groupe;

/**
 * Groupe routes tests
 */
describe('Groupe CRUD tests', function() {
	beforeEach(function(done) {
		// Create user credentials
		credentials = {
			username: 'username',
			password: 'password'
		};

		// Create a new user
		user = new User({
			firstName: 'Full',
			lastName: 'Name',
			displayName: 'Full Name',
			email: 'test@test.com',
			username: credentials.username,
			password: credentials.password,
			provider: 'local'
		});

		// Save a user to the test db and create new Groupe
		user.save(function() {
			groupe = {
				name: 'Groupe Name'
			};

			done();
		});
	});

	it('should be able to save Groupe instance if logged in', function(done) {
		agent.post('/auth/signin')
			.send(credentials)
			.expect(200)
			.end(function(signinErr, signinRes) {
				// Handle signin error
				if (signinErr) done(signinErr);

				// Get the userId
				var userId = user.id;

				// Save a new Groupe
				agent.post('/groupes')
					.send(groupe)
					.expect(200)
					.end(function(groupeSaveErr, groupeSaveRes) {
						// Handle Groupe save error
						if (groupeSaveErr) done(groupeSaveErr);

						// Get a list of Groupes
						agent.get('/groupes')
							.end(function(groupesGetErr, groupesGetRes) {
								// Handle Groupe save error
								if (groupesGetErr) done(groupesGetErr);

								// Get Groupes list
								var groupes = groupesGetRes.body;

								// Set assertions
								(groupes[0].user._id).should.equal(userId);
								(groupes[0].name).should.match('Groupe Name');

								// Call the assertion callback
								done();
							});
					});
			});
	});

	it('should not be able to save Groupe instance if not logged in', function(done) {
		agent.post('/groupes')
			.send(groupe)
			.expect(401)
			.end(function(groupeSaveErr, groupeSaveRes) {
				// Call the assertion callback
				done(groupeSaveErr);
			});
	});

	it('should not be able to save Groupe instance if no name is provided', function(done) {
		// Invalidate name field
		groupe.name = '';

		agent.post('/auth/signin')
			.send(credentials)
			.expect(200)
			.end(function(signinErr, signinRes) {
				// Handle signin error
				if (signinErr) done(signinErr);

				// Get the userId
				var userId = user.id;

				// Save a new Groupe
				agent.post('/groupes')
					.send(groupe)
					.expect(400)
					.end(function(groupeSaveErr, groupeSaveRes) {
						// Set message assertion
						(groupeSaveRes.body.message).should.match('Please fill Groupe name');
						
						// Handle Groupe save error
						done(groupeSaveErr);
					});
			});
	});

	it('should be able to update Groupe instance if signed in', function(done) {
		agent.post('/auth/signin')
			.send(credentials)
			.expect(200)
			.end(function(signinErr, signinRes) {
				// Handle signin error
				if (signinErr) done(signinErr);

				// Get the userId
				var userId = user.id;

				// Save a new Groupe
				agent.post('/groupes')
					.send(groupe)
					.expect(200)
					.end(function(groupeSaveErr, groupeSaveRes) {
						// Handle Groupe save error
						if (groupeSaveErr) done(groupeSaveErr);

						// Update Groupe name
						groupe.name = 'WHY YOU GOTTA BE SO MEAN?';

						// Update existing Groupe
						agent.put('/groupes/' + groupeSaveRes.body._id)
							.send(groupe)
							.expect(200)
							.end(function(groupeUpdateErr, groupeUpdateRes) {
								// Handle Groupe update error
								if (groupeUpdateErr) done(groupeUpdateErr);

								// Set assertions
								(groupeUpdateRes.body._id).should.equal(groupeSaveRes.body._id);
								(groupeUpdateRes.body.name).should.match('WHY YOU GOTTA BE SO MEAN?');

								// Call the assertion callback
								done();
							});
					});
			});
	});

	it('should be able to get a list of Groupes if not signed in', function(done) {
		// Create new Groupe model instance
		var groupeObj = new Groupe(groupe);

		// Save the Groupe
		groupeObj.save(function() {
			// Request Groupes
			request(app).get('/groupes')
				.end(function(req, res) {
					// Set assertion
					res.body.should.be.an.Array.with.lengthOf(1);

					// Call the assertion callback
					done();
				});

		});
	});


	it('should be able to get a single Groupe if not signed in', function(done) {
		// Create new Groupe model instance
		var groupeObj = new Groupe(groupe);

		// Save the Groupe
		groupeObj.save(function() {
			request(app).get('/groupes/' + groupeObj._id)
				.end(function(req, res) {
					// Set assertion
					res.body.should.be.an.Object.with.property('name', groupe.name);

					// Call the assertion callback
					done();
				});
		});
	});

	it('should be able to delete Groupe instance if signed in', function(done) {
		agent.post('/auth/signin')
			.send(credentials)
			.expect(200)
			.end(function(signinErr, signinRes) {
				// Handle signin error
				if (signinErr) done(signinErr);

				// Get the userId
				var userId = user.id;

				// Save a new Groupe
				agent.post('/groupes')
					.send(groupe)
					.expect(200)
					.end(function(groupeSaveErr, groupeSaveRes) {
						// Handle Groupe save error
						if (groupeSaveErr) done(groupeSaveErr);

						// Delete existing Groupe
						agent.delete('/groupes/' + groupeSaveRes.body._id)
							.send(groupe)
							.expect(200)
							.end(function(groupeDeleteErr, groupeDeleteRes) {
								// Handle Groupe error error
								if (groupeDeleteErr) done(groupeDeleteErr);

								// Set assertions
								(groupeDeleteRes.body._id).should.equal(groupeSaveRes.body._id);

								// Call the assertion callback
								done();
							});
					});
			});
	});

	it('should not be able to delete Groupe instance if not signed in', function(done) {
		// Set Groupe user 
		groupe.user = user;

		// Create new Groupe model instance
		var groupeObj = new Groupe(groupe);

		// Save the Groupe
		groupeObj.save(function() {
			// Try deleting Groupe
			request(app).delete('/groupes/' + groupeObj._id)
			.expect(401)
			.end(function(groupeDeleteErr, groupeDeleteRes) {
				// Set message assertion
				(groupeDeleteRes.body.message).should.match('User is not logged in');

				// Handle Groupe error error
				done(groupeDeleteErr);
			});

		});
	});

	afterEach(function(done) {
		User.remove().exec();
		Groupe.remove().exec();
		done();
	});
});