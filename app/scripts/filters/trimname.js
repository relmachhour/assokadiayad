'use strict';

/**
 * @ngdoc filter
 * @name demoApp.filter:trimName
 * @function
 * @description
 * # trimName
 * Filter in the demoApp.
 */
angular.module('demoApp')
  .filter('trimName', function () {
    return function (input) {
    	var str = input.trim();
    	str = str.replace(' ','-')
      return str;
    };
  });
