'use strick';




/***
* package dependencies
* @author fayssal tahtoub
* @email <fayssal.tahtoub@gmail.com>
*/
var _ = require('lodash');


/**
* @class ObjectManager manage all object
* @constructor of ObjectManager
*
*/
var ObjectManager= function(){
};


/**
 * @method mongooseToJSONObject casting from mongoose object to json object
 * @static
 * @param {Object}
 * @return {JSONObject}
 */
ObjectManager.mongooseToJSONObject= function(value){
  return JSON.parse(JSON.stringify(value));
};

/**
 * @method deleteProperties delete all properties in highstack from the object
 * @static
 * @param {Object}
 * @param {Array}
 *
 * @return {Object}
 */
ObjectManager.deleteProperties= function(obj, stack){

  for(var i= 0;i < stack.length;i++)
    obj = _.omit(obj,stack[i]);

  return obj;
};


/**
 * @method convertArrayToAssoc
 * @static
 * @param {Array}
 *
 * @return {Object}
 */
ObjectManager.convertArrayToMap= function(stack){
  var map = {};
  _.each(stack,function(item,indx){
    map[''+indx] = item;
  });

  return map;
};

/**
 * @method toAbusFormat convert the gived object to the supported abus format
 * @static
 * @param {Array}
 *
 * @return {Object}
 */
ObjectManager.toAbusString= function(data){
  var abusObject = ObjectManager.checkAndConvertToAbusFormat(data);
  return JSON.stringify(abusObject);
};


/**
* @method checkAndConvertToAbusFormat check and convert if the format is valid to abus support
*
*/
ObjectManager.checkAndConvertToAbusFormat = function(data){

  if(_.isArray(data)){
    return ObjectManager.checkAndConvertToAbusFormat(ObjectManager.convertArrayToMap(data));
  }else if(_.isObject(data)){
    var obj = {}
    _.forIn(data,function(value,key){
      obj[key] = ObjectManager.checkAndConvertToAbusFormat(value);
    });
    return obj;
  }

  return data;
};


/**
 * Export object
 *
 */
module.exports.ObjectManager = ObjectManager;
