'use strict';

/**
 * Module dependencies.
 */
var users = require('../../app/controllers/users.server.controller'),
	dashboard = require('../../app/controllers/dashboard.server.controller');

module.exports = function(app) {
	// Expense Routes 
	app.route('/api/dashboard/report')
		.get(users.hasAuthorization(['ROLE_SUPER_ROOT','ROLE_SUPER_ADMIN','ROLE_ADMIN','ROLE_USER']),dashboard.dashReport)
	app.route('/api/hisotry/find')
		.post(users.hasAuthorization(['ROLE_SUPER_ROOT','ROLE_SUPER_ADMIN','ROLE_ADMIN','ROLE_USER']),dashboard.find)

	app.route('/api/hisotry/export')
		.post(users.hasAuthorization(['ROLE_SUPER_ROOT','ROLE_SUPER_ADMIN','ROLE_ADMIN','ROLE_USER']),dashboard.down)
	


	app.route('/api/hisotry/funds-table')
		.post(users.hasAuthorization(['ROLE_SUPER_ROOT','ROLE_SUPER_ADMIN','ROLE_ADMIN','ROLE_USER']),dashboard.fundTable)

	app.route('/api/hisotry/banks-table')
		.post(users.hasAuthorization(['ROLE_SUPER_ROOT','ROLE_SUPER_ADMIN','ROLE_ADMIN','ROLE_USER']),dashboard.bankTable)

	app.route('/api/hisotry/incomes-table')
		.post(users.hasAuthorization(['ROLE_SUPER_ROOT','ROLE_SUPER_ADMIN','ROLE_ADMIN','ROLE_USER']),dashboard.incomeTable)

	app.route('/api/hisotry/expenses-table')
		.post(users.hasAuthorization(['ROLE_SUPER_ROOT','ROLE_SUPER_ADMIN','ROLE_ADMIN','ROLE_USER']),dashboard.expenseTable)

	app.route('/api/hisotry/student-participation')
		.post(users.hasAuthorization(['ROLE_SUPER_ROOT','ROLE_SUPER_ADMIN','ROLE_ADMIN','ROLE_USER']),dashboard.studentParticipation)





};
