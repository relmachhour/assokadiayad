'use strict';

/**
 * Module dependencies.
 */
var users = require('../../app/controllers/users.server.controller'),
	type_incomes = require('../../app/controllers/type_incomes.server.controller');

module.exports = function(app) {
	// Type_income Routes 
	app.route('/type_incomes')
		.get(users.hasAuthorization(['ROLE_SUPER_ROOT','ROLE_SUPER_ADMIN','ROLE_ADMIN','ROLE_USER']),type_incomes.list)
		.post(users.hasAuthorization(['ROLE_SUPER_ROOT','ROLE_SUPER_ADMIN','ROLE_ADMIN','ROLE_USER']),type_incomes.create);

	app.route('/type_incomes/:type_incomeId')
		.get(type_incomes.read)
		.put(users.hasAuthorization(['ROLE_SUPER_ROOT','ROLE_SUPER_ADMIN','ROLE_ADMIN','ROLE_USER']),type_incomes.update)
		.delete(users.hasAuthorization(['ROLE_SUPER_ROOT','ROLE_SUPER_ADMIN','ROLE_ADMIN','ROLE_USER']),type_incomes.delete);

	// Finish by binding the type_income middleware
	app.param('type_incomeId', type_incomes.type_incomeByID);
};
