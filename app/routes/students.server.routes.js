'use strict';

/**
 * Module dependencies.
 */
var users = require('../../app/controllers/users.server.controller'),
	students = require('../../app/controllers/students.server.controller');

module.exports = function(app) {
	// Student Routes 
	app.route('/students')
		.get(users.hasAuthorization(['ROLE_SUPER_ROOT','ROLE_SUPER_ADMIN','ROLE_ADMIN','ROLE_USER']),students.list)
		.post(users.hasAuthorization(['ROLE_SUPER_ROOT','ROLE_SUPER_ADMIN','ROLE_ADMIN','ROLE_USER']),students.create);

	app.route('/students/:studentId')
		.get(students.read)
		.put(users.hasAuthorization(['ROLE_SUPER_ROOT','ROLE_SUPER_ADMIN','ROLE_ADMIN','ROLE_USER']),students.update)
		.delete(users.hasAuthorization(['ROLE_SUPER_ROOT','ROLE_SUPER_ADMIN','ROLE_ADMIN','ROLE_USER']),students.delete);

	// Finish by binding the student middleware
	app.param('studentId', students.studentByID);
};
