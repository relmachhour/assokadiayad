'use strict';

/**
 * Module dependencies.
 */
var users = require('../../app/controllers/users.server.controller'),
	classes = require('../../app/controllers/classes.server.controller');

module.exports = function(app) {
	// Class Routes 
	app.route('/classes')
		.get(users.hasAuthorization(['ROLE_SUPER_ROOT','ROLE_SUPER_ADMIN','ROLE_ADMIN','ROLE_USER']),classes.list)
		.post(users.hasAuthorization(['ROLE_SUPER_ROOT','ROLE_SUPER_ADMIN','ROLE_ADMIN','ROLE_USER']),classes.create);

	app.route('/classes/:classId')
		.get(classes.read)
		.put(users.hasAuthorization(['ROLE_SUPER_ROOT','ROLE_SUPER_ADMIN','ROLE_ADMIN','ROLE_USER']),classes.update)
		.delete(users.hasAuthorization(['ROLE_SUPER_ROOT','ROLE_SUPER_ADMIN','ROLE_ADMIN','ROLE_USER']),classes.delete);

	// Finish by binding the class middleware
	app.param('classId', classes.classVarByID);
};
