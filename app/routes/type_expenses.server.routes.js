'use strict';

/**
 * Module dependencies.
 */
var users = require('../../app/controllers/users.server.controller'),
	type_expenses = require('../../app/controllers/type_expenses.server.controller');

module.exports = function(app) {
	// Type_expense Routes 
	app.route('/type_expenses')
		.get(users.hasAuthorization(['ROLE_SUPER_ROOT','ROLE_SUPER_ADMIN','ROLE_ADMIN','ROLE_USER']),type_expenses.list)
		.post(users.hasAuthorization(['ROLE_SUPER_ROOT','ROLE_SUPER_ADMIN','ROLE_ADMIN','ROLE_USER']),type_expenses.create);

	app.route('/type_expenses/:type_expenseId')
		.get(type_expenses.read)
		.put(users.hasAuthorization(['ROLE_SUPER_ROOT','ROLE_SUPER_ADMIN','ROLE_ADMIN','ROLE_USER']),type_expenses.update)
		.delete(users.hasAuthorization(['ROLE_SUPER_ROOT','ROLE_SUPER_ADMIN','ROLE_ADMIN','ROLE_USER']),type_expenses.delete);

	// Finish by binding the type_expense middleware
	app.param('type_expenseId', type_expenses.type_expenseByID);
};
