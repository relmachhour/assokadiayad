'use strict';

/**
 * Module dependencies.
 */
var users = require('../../app/controllers/users.server.controller'),
	incomes = require('../../app/controllers/incomes.server.controller');

module.exports = function(app) {
	// Income Routes 
	app.route('/incomes')
		.get(users.hasAuthorization(['ROLE_SUPER_ROOT','ROLE_SUPER_ADMIN','ROLE_ADMIN','ROLE_USER']),incomes.list)
		.post(users.hasAuthorization(['ROLE_SUPER_ROOT','ROLE_SUPER_ADMIN','ROLE_ADMIN','ROLE_USER']),incomes.create);

	app.route('/incomes/:incomeId')
		.get(incomes.read)
		.put(users.hasAuthorization(['ROLE_SUPER_ROOT','ROLE_SUPER_ADMIN','ROLE_ADMIN','ROLE_USER']),incomes.update)
		.delete(users.hasAuthorization(['ROLE_SUPER_ROOT','ROLE_SUPER_ADMIN','ROLE_ADMIN','ROLE_USER']),incomes.delete);

	// Finish by binding the income middleware
	app.param('incomeId', incomes.incomeByID);
};
