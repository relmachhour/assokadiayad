module.exports= function(app){
  var users= require('../../app/controllers/users.server.controller');
  var admins= require('../../app/controllers/admins.server.controller');

  app.route('/api/admins')
    .get(users.hasAuthorization(['ROLE_SUPER_ADMIN','ROLE_ADMIN']),admins.list)
    .post(users.hasAuthorization(['ROLE_SUPER_ADMIN','ROLE_ADMIN']),admins.create);

  app.route('/api/admins/:adminId')
    .get(users.hasAuthorization(['ROLE_SUPER_ADMIN','ROLE_ADMIN']),admins.read)
    .put(users.hasAuthorization(['ROLE_SUPER_ADMIN','ROLE_ADMIN']),admins.update)
    .delete(users.hasAuthorization(['ROLE_SUPER_ADMIN','ROLE_ADMIN']),admins.delete);


  app.route('/api/admins/:adminId/switch-state')
    .post(users.hasAuthorization(['ROLE_SUPER_ADMIN','ROLE_ADMIN']),admins.switchState);

  app.route('/api/admins/role')
    .post(users.hasAuthorization(['ROLE_SUPER_ADMIN','ROLE_ADMIN','ROLE_USER']),admins.getRole);

  app.route('/api/profil')
    .get(users.hasAuthorization(['ROLE_SUPER_ADMIN','ROLE_ADMIN','ROLE_USER']),admins.getProfil)
    .post(users.hasAuthorization(['ROLE_SUPER_ADMIN','ROLE_ADMIN','ROLE_USER']),admins.updateProfil);

  app.param('adminId',admins.adminById);
};
