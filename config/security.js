'use strict';

module.exports = {
  acl: {
    ROLE_SUPER_ADMIN: ['ROLE_ADMIN','ROLE_ALLOW_TO_SWITCH'],
    ROLE_ADMIN: ['ROLE_USER'],
    ROLE_SALES_AGENT: ['ROLE_USER'],
    ROLE_COMPANY_MANAGER: ['ROLE_COMPANY_AGENT'],
    ROLE_COMPANY_AGENT: ['ROLE_ALLOWED_TO_READ','ROLE_ALLOWED_TO_WRITE'],
    ROLE_ALLOWED_TO_READ: ['ROLE_USER'],
    ROLE_ALLOWED_TO_WRITE: ['ROLE_USER']
  },
  auth_providers:[
    {name:'super-user', type:'memory', username:'admin', password:'001985',enabled:true},
    {name:'local',type:'passport', path:'',enabled:true},
  ],
  firewalls:[
    {id:'dev_resource', resource:'^/api/devices', allow:['ROLE_COMPANY_AGENT']},
    {parent:'dev_resource', resource:'^/api/devices', allow:['ROLE_COMPANY_AGENT'],inherits:false},
    {resource:'^/api/company/user', deny:['ROLE_COMPANY_AGENT']},
    {resource:'^/api/company/user', allow:['ROLE_COMPANY_AGENT'],deny:['ROLE_COMPANY_AGENT']},
  ]
};
