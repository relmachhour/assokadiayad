'use strict';

/**
 * Module dependencies.
 */
var glob = require('glob'),
	chalk = require('chalk');

/**
 * Module init function.
 */
module.exports = function() {
	/**
	 * Before we begin, lets set the environment variable
	 * We'll Look for a valid NODE_ENV variable and if one cannot be found load the development NODE_ENV
	 */
	/*glob('./config/env/' + process.env.NODE_ENV + '.js', {
		sync: true
	}, function(err, environmentFiles) {
		if (!environmentFiles.length) {
			if (process.env.NODE_ENV) {
				console.error(chalk.red('No configuration file found for "' + process.env.NODE_ENV + '" environment using development instead'));
			} else {
				console.error(chalk.red('NODE_ENV is not defined! Using default development environment'));
			}

			process.env.NODE_ENV = 'development';
		} else {
			console.log(chalk.black.bgWhite('Application loaded using the "' + process.env.NODE_ENV + '" environment configuration'));
		}
	});*/

//	process.env.NODE_ENV = 'all'
	  var environmentFiles = glob.sync('./config/env/' + process.env.NODE_ENV + '.js');
	  console.log('environmentFiles');
	  console.log();
	  console.log(environmentFiles);
	  if (!environmentFiles.length) {
	  	console.log('--!environmentFiles.length---')
	    if (process.env.NODE_ENV) {
	   		console.log('--process.env.NODE_ENV---',process.env.NODE_ENV)

	      console.error(chalk.red('+ Error: No configuration file found for "' + process.env.NODE_ENV + '" environment using development instead'));
	    } else {
	    	console.log('--ELSE process.env.NODE_ENV---')
	      console.error(chalk.red('+ Error: NODE_ENV is not defined! Using default development environment'));
	    }

	    process.env.NODE_ENV = 'development';
	    environmentFiles = glob.sync('./config/env/' + process.env.NODE_ENV + '.js');
	    console.log(environmentFiles)
	  }
	  // Reset console color
	  console.log(chalk.white(''));




/*

var path = require('path')
var Glob = require('glob').Glob

var pattern =  path.join(__dirname, '/env/development.js');

console.log(pattern)

var mg = new Glob(pattern, {sync:true})
console.log("after")

*/





};