const { Pool, Client } = require('pg'),
      config    = require('../config');


const client = new Client({
  user: config.db.username, 
  host: config.db.host,
  database: config.db.name,
  password: config.db.password,
  port: config.db.port,
})

client.connect()

exports.pg = client;




