'use strict';

/**
 * Module dependencies.
 */
var passport = require('passport'),
	LocalStrategy = require('passport-local').Strategy,
	config = require('../config'),
	User = require('mongoose').model('User');

var jwt = require('jwt-simple');

module.exports = function() {
	// Use local strategy
	passport.use(new LocalStrategy({
			usernameField: 'email',
			passwordField: 'password'
		},
		function(email, password, done) {
			User.findOne({
				email: email
			}, function(err, user) {
				if (err) {
					return done(err);
				}
				if (!user) {
					return done(null, false, {
						message: 'Unknown user or invalid password !!'
					});
				}
				if (!user.authenticate(password)) {
					return done(null, false, {
						message: 'Unknown user or invalid password !!'
					});
				}

				return done(null, user);
			});
		}
	));

	passport.use('local-token', new LocalStrategy({
			usernameField: 'email',
			passwordField: 'password'
		},
		function(email, password, done) {

			User.findOne({
				email: email
			}, function(err, user) {
				if (err) {
					console.log(err);
					return done(err);
				}
				if (!user) {
					return done(null, false, {
						message: 'Oops: Unknown user or invalid password!!!'
					});
				}
				if (!user.authenticate(password)) {
					return done(null, false, {
						message: 'Oops: Unknown user or invalid password!!!'
					});
				}
				//console.log('body : '+req.body);
				// token expire time
				/*var expireTime = config.getExpireTokenTime();

				// generate login token
				var tokenPayload = {
					email: user.email,
					expires: expireTime
				};

				var token = jwt.encode(tokenPayload, config.jwtTokenSecret, config.jwtHashMethod);

				// add token and exp date to user object
				user.token = token;
				user.expires = expireTime;

				// save user object to update database
				user.save(function(err) {
					if(err){
						done(err);
					} else {
						done(null, user);
					}
				});*/
				done(null, user);

			});
		}
	));
};
