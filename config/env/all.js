/*
'use strict';

module.exports = {
    app: {
        title: 'remote-trucks.com',
        description: 'remote-trucks.com is fullstack architecture that help manager to control there logistique',
        keywords: 'truck, bus, cars, remote'
    },
    port: process.env.PORT || 3000,
    templateEngine: 'swig',
    sessionSecret: 'MEAN',
    sessionCollection: 'sessions',
    jwtTokenSecret: 'b3fc1aedb94bc31469fac62e9e2938b94c978d32',
    jwtHashMethod: 'HS512', // supported algo HS256, HS384, HS512 and RS256

    jwtExpireTime: '8 hours, 30 minutes',
    jwtTokenName: 'token',
    jwtTokenSigninPath: '/api/security/auth/signin',
    maxTimePasswordReset: '1 hours',

    assets: {
        lib: {
            css: [

                'public/dist/vendor.min.css',

            ],
            js: [
                'public/dist/vendor.min.js',
                'public/scripts/alertify.min.js'

            ]
        },
        css: [
             'public/dist/application.min.css',
             'public/styles/custom.css'

        ],
        js: [
            'public/dist/application.min.js',
            'public/dist/templates.min.js'
        ],
    }
};


*/




'use strict';

module.exports = {
    app: {
        title: 'remote-trucks.com',
        description: 'remote-trucks.com is fullstack architecture that help manager to control there logistique',
        keywords: 'truck, bus, cars, remote'
    },
    port: process.env.PORT || 3000,
    templateEngine: 'swig',
    sessionSecret: 'MEAN',
    sessionCollection: 'sessions',
    jwtTokenSecret: 'b3fc1aedb94bc31469fac62e9e2938b94c978d32',
    jwtHashMethod: 'HS512', // supported algo HS256, HS384, HS512 and RS256
    /**
        ***** Expire Time help ******
        Format like this: 1 years, 3 days, 30 minutes
        Supported instruction: years, quarters, months, weeks, days, hours, minutes, seconds, milliseconds
    */
    jwtExpireTime: '8 hours, 30 minutes',
    jwtTokenName: 'token',
    jwtTokenSigninPath: '/api/security/auth/signin',
    maxTimePasswordReset: '1 hours',

    assets: {
        lib: {
            css: [
                'public/lib/bootstrap/dist/css/bootstrap.min.css',
                'public/lib/font-awesome/css/font-awesome.min.css',

                'public/styles/custom.css',


               'public/scripts/chosen/chosen.min.css',


                'public/scripts/alertify/alertify.min.css',

                 'public/styles/editedcss.css',

            ],
            js: [

                'public/lib/jquery/dist/jquery.min.js',
                'public/lib/underscore/underscore-min.js',
                'public/lib/bootstrap/dist/js/bootstrap.min.js',

                'public/scripts/chosen/chosen.jquery.min.js',
                'public/lib/chart.js/dist/Chart.min.js',
                'public/scripts/html2canvas.min.js',


                'public/lib/angular/angular.min.js',
                'public/lib/angular-animate/angular-animate.min.js',
                'public/lib/angular-cookies/angular-cookies.min.js',
                'public/lib/angular-resource/angular-resource.min.js',
                'public/lib/angular-sanitize/angular-sanitize.min.js',
                'public/lib/angular-touch/angular-touch.min.js',
                'public/lib/angular-local-storage/dist/angular-local-storage.min.js',
                'public/lib/angular-ui-router/release/angular-ui-router.min.js',
                'public/lib/angular-ui-router.stateHelper/statehelper.min.js',
                'public/lib/angular-socket-io/socket.min.js',
                'public/lib/angular-bootstrap/ui-bootstrap.min.js',
                'public/lib/angular-bootstrap/ui-bootstrap-tpls.js',
                'public/lib/angular-chart.js/dist/angular-chart.min.js',

                'public/scripts/chosen/angular-chosen.min.js',

                'public/scripts/alertify/alertify.min.js'



              
            ]
        },
        css: [
            //'public/modules/**/css/*.css'
        ],
        js: [
            'public/config.js',
            'public/application.js',
            'public/modules/*/*.js',
            'public/modules/*/*[!tests]*/*.js'
        ],
        tests: [
            'public/lib/angular-mocks/angular-mocks.js',
            'public/modules/*/tests/*.js'
        ]
    }
};
