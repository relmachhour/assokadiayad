'use strict';

module.exports = {
	db: {
		name: process.env.DB_NAME || "ayad",
	    host: process.env.DB_HOST || "localhost",
	    port: process.env.DB_PORT || 5432,
	    username: process.env.DB_USERNAME || "rachid",
	    password: process.env.DB_PASSWORD || "rachid",
	    dialect: process.env.DB_DIALECT || "postgres", //mysql, postgres, sqlite3,...
	    enableSequelizeLog: process.env.DB_LOG || false,
	    ssl: process.env.DB_SSL || false,
	    sync: process.env.DB_SYNC || true //Synchronizing any model changes with database
	},
	app: {
		title: 'demo - Development Environment'
	},
	facebook: {
		clientID: process.env.FACEBOOK_ID || 'APP_ID',
		clientSecret: process.env.FACEBOOK_SECRET || 'APP_SECRET',
		callbackURL: '/auth/facebook/callback'
	},
	twitter: {
		clientID: process.env.TWITTER_KEY || 'CONSUMER_KEY',
		clientSecret: process.env.TWITTER_SECRET || 'CONSUMER_SECRET',
		callbackURL: '/auth/twitter/callback'
	},
	google: {
		clientID: process.env.GOOGLE_ID || 'APP_ID',
		clientSecret: process.env.GOOGLE_SECRET || 'APP_SECRET',
		callbackURL: '/auth/google/callback'
	},
	linkedin: {
		clientID: process.env.LINKEDIN_ID || 'APP_ID',
		clientSecret: process.env.LINKEDIN_SECRET || 'APP_SECRET',
		callbackURL: '/auth/linkedin/callback'
	},
	github: {
		clientID: process.env.GITHUB_ID || 'APP_ID',
		clientSecret: process.env.GITHUB_SECRET || 'APP_SECRET',
		callbackURL: '/auth/github/callback'
	},
	mailer: {
		from: process.env.MAILER_FROM || 'esvitech@gmail.com',
		options: {
			service: process.env.MAILER_SERVICE_PROVIDER || 'Gmail',
			auth: {
				user: process.env.MAILER_EMAIL_ID || 'esvitech@gmail.com',
				pass: process.env.MAILER_PASSWORD || 'esvitech2015'
			}
		}
	}
};
