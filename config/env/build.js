
'use strict';

module.exports = {
    app: {
        title: 'remote-trucks.com',
        description: 'remote-trucks.com is fullstack architecture that help manager to control there logistique',
        keywords: 'truck, bus, cars, remote'
    },
    port: process.env.PORT || 3000,
    templateEngine: 'swig',
    sessionSecret: 'MEAN',
    sessionCollection: 'sessions',
    jwtTokenSecret: 'b3fc1aedb94bc31469fac62e9e2938b94c978d32',
    jwtHashMethod: 'HS512', // supported algo HS256, HS384, HS512 and RS256
    /**
        ***** Expire Time help ******
        Format like this: 1 years, 3 days, 30 minutes
        Supported instruction: years, quarters, months, weeks, days, hours, minutes, seconds, milliseconds
    */
    jwtExpireTime: '8 hours, 30 minutes',
    jwtTokenName: 'token',
    jwtTokenSigninPath: '/api/security/auth/signin',
    maxTimePasswordReset: '1 hours',

    assets: {
        lib: {
            css: [

                'public/lib/bootstrap/dist/css/bootstrap.min.css',

                'public/lib/angular-bootstrap-calendar/dist/css/angular-bootstrap-calendar.min.css', // calendar css*/
                'public/lib/ocModal/dist/css/ocModal.light.css', // modal open
                'public/lib/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.min.css',
                'public/lib/angular-ui-bootstrap-datetimepicker/datetimepicker.css',

                'public/styles/layout-designer.css',
                'public/styles/main.css',

                //'public/styles/custom.css',



                'public/lib/jquery.splitter/css/jquery.splitter.css', // splitter

                'public/lib/fullcalendar/dist/fullcalendar.min.css', // fullcalendar
                'public/styles/fullcalendar.custom.css', // fullcalendar
                'public/styles/jquery.dropdown.css',
                'public/styles/alertify.min.css',
                'public/styles/semantic.min.css',
                'public/styles/spectrum.css'





            ],
            js: [
                'public/lib/jquery/dist/jquery.min.js',
                'public/lib/jquery-mousewheel/jquery.mousewheel.min.js',
                'public/lib/moment/min/moment.min.js',
                'public/lib/lodash/lodash.min.js',
                'public/lib/jquery-ui/jquery-ui.min.js',
                'public/style/myjs/jquery.ui.widget.min.js',
                'public/lib/jquery.easing/js/jquery.easing.min.js', //added for easing
                //  'public/lib/Chart.js/Chart.min.js',
                'public/lib/bootstrap/dist/js/bootstrap.min.js',
                'public/lib/bootstrap-select/js/bootstrap-select.js',
                'public/lib/remarkable-bootstrap-notify/dist/bootstrap-notify.js', // bootstrap-notify
                //  'public/lib/jquery.scrollbar/jquery.scrollbar.min.js',
                'public/lib/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min.js',
                ///////////////////////////////////////////////////




                // add to support fullcalendar
                'public/lib/fullcalendar/dist/fullcalendar.js',
                'public/lib/fullcalendar/dist/gcal.js',

                //      '//maps.googleapis.com/maps/api/js?sensor=false',// include google maps v3 

                'public/lib/jquery.splitter/js/jquery.splitter.js', // splitter

                ///////////////////////////////////////////////////




                'public/lib/angular/angular.min.js',
                'public/lib/angular-resource/angular-resource.min.js',
                'public/lib/angular-cookies/angular-cookies.min.js',
                'public/lib/angular-animate/angular-animate.min.js',
                'public/lib/angular-touch/angular-touch.min.js',
                'public/lib/angular-sanitize/angular-sanitize.min.js',
                'public/lib/angular-ui-router/release/angular-ui-router.min.js',
                'public/lib/angular-ui-utils/ui-utils.min.js',
                'public/lib/angular-bootstrap/ui-bootstrap-tpls.min.js',
                'public/lib/angular-ui-router.stateHelper/statehelper.min.js',
                //  'public/lib/angular-chart.js/dist/angular-chart.js',
                'public/lib/angular-moment/angular-moment.min.js',
                'public/lib/angular-file-upload/dist/angular-file-upload.min.js',
                'public/lib/angular-click-outside/clickoutside.directive.js',

                //'public/lib/angular-ui-bootstrap-datetimepicker/datetimepicker.js',
                'public/scripts/datetimepicker_ui.js',

                'public/scripts/spectrum.js',
                'public/scripts/angular-spectrum-colorpicker.min.js',




                'public/lib/angular-socket-io/mock/socket-io.js',
                'public/lib/angular-socket-io/socket.min.js',


                'public/scripts/angular-bootstrap-calendar-tpls-modif.js',
                'public/scripts/moment-fr.js',

                'public/lib/ocModal/dist/ocModal.min.js', // modal open




                // add full calendar support
                'public/lib/angular-ui-calendar/src/calendar.js',

                // google maps
                'public/lib/angular-simple-logger/dist/angular-simple-logger.js',
                'public/lib/angular-google-maps/dist/angular-google-maps.js',




                'public/lib/Chart.js/Chart.js',
                'public/lib/angular-chart.js/dist/angular-chart.min.js',



                'public/scripts/jquery.dropdown.js',
                // my scripts
                'public/scripts/angular-locale_fr-fr.js',
                'public/scripts/main.js',
                'public/scripts/jquery-layout-designer.js',
                'public/scripts/angular-ui-task-programmer.js',
               // 'public/scripts/imgViewer.js',
                'public/scripts/alertify.min.js',
                'public/scripts/ui-bootstrap-tpls-2.5.0.min.js'

            ]
        },
        css: [
            //'public/modules/**/css/*.css'
        ],
        js: [
            'public/config.js',
            'public/application.js',
            'public/modules/*/*.js',
            'public/modules/*/*[!tests]*/*.js'
        ],
        tests: [
            'public/lib/angular-mocks/angular-mocks.js',
            'public/modules/*/tests/*.js'
        ]
    }
};