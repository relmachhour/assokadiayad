'use strict';

// Init the application configuration module for AngularJS application
var ApplicationConfiguration = (function() {
	// Init module configuration options
	var applicationModuleName = 'ProSignagE';
	var applicationModuleVendorDependencies = ['btford.socket-io','ngResource', 'ngCookies',  'ngTouch',  'ngSanitize',  'ui.router', 'ui.router.stateHelper' , 'ui.bootstrap'];

	// Add a new vertical module
	var registerModule = function(moduleName, dependencies) {
		// Create angular module
		var app= angular.module(moduleName, dependencies || []);

		app.run(function ($rootScope, $location, $state, Authentication) {
			$rootScope.$on('$stateChangeStart', function (event, toState, toParams) {
				var security = toState.security;
				if(security != undefined){
					if(security.requiredAuth){

						var token = localStorage.getItem('token')
						var role = localStorage.getItem('roles')

						if(token!='undefined'){
							token = localStorage.getItem('token');
						}
						
						if(role!='undefined'){
						 	role  = localStorage.getItem('roles');
						}
						if(typeof token === 'string' && token!='undefined'){
							if(security.requiredPermissions != undefined){
								if(!security.requiredPermissions.includes(role)){
									event.preventDefault();
									$state.go('unauthorized');
								}
							}
						}else{
							event.preventDefault();
							$state.go('signin');
						}

					}

				}
			});
		});

		// Add the module to the AngularJS configuration file
		angular.module(applicationModuleName).requires.push(moduleName);
	};

	return {
		applicationModuleName: applicationModuleName,
		applicationModuleVendorDependencies: applicationModuleVendorDependencies,
		registerModule: registerModule
	};
})();
