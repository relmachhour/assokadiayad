'use strict';

// Setting up route
angular.module('banks').config(['$stateProvider',
	function($stateProvider) {
		// Articles state routing
		$stateProvider.
		state('backoffice.admin.listBanks', {
			url: '/banks',
			views:{
				"content@backoffice": {
					templateUrl: 'modules/banks/views/list-banks.view.html',
					controller: 'BanksController'
				}
			},
			security:{
				requiredAuth: true,
				requiredPermissions:  ['ROLE_SUPER_ROOT','ROLE_SUPER_ADMIN','ROLE_ADMIN','ROLE_USER']
			}	
		}).
		state('backoffice.admin.createBanks', {
			url: '/banks/create',
			views:{
				"content@backoffice": {
					templateUrl: 'modules/banks/views/create-bank.view.html',
					controller: 'BanksController'
				}
			},
			security:{
				requiredAuth: true,
				requiredPermissions:  ['ROLE_SUPER_ROOT','ROLE_SUPER_ADMIN','ROLE_ADMIN','ROLE_USER']
			}	
		}).
		state('backoffice.admin.editBanks', {
			url: '/banks/:bankId/edit',
			views:{
				"content@backoffice": {
					templateUrl: 'modules/banks/views/edit-bank.view.html',
					controller: 'BanksController'
				}
			},
			security:{
				requiredAuth: true,
				requiredPermissions:  ['ROLE_SUPER_ROOT','ROLE_SUPER_ADMIN','ROLE_ADMIN','ROLE_USER']
			}	
		})
		;
	}
]);
