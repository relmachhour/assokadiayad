
'use strict';


angular.module('banks').controller('BanksController', ['$scope', '$rootScope', '$http','$state' ,'$stateParams', '$location', 'Authentication','$uibModal','Banks',
    function($scope, $rootScope, $http,$state, $stateParams, $location, Authentication,$uibModal,Banks) {

        $scope.newBank = {};

        $scope.find = function()
        {
            Banks.query().$promise.then(function(result) {
                $scope.banks = result;
            }); 
        }
        $scope.find()
        
        $scope.findOne = function()
        {
            Banks.get({
                bankId: $stateParams.bankId
            }).$promise.then(function(result) {
                    $scope.bank = result
            }, function(error) {
                console.log(error)
            })
        }



        $scope.saveNewBank = function()
        {
            var saveBank = new Banks($scope.newBank) 

            saveBank.$save(function(res) {
                console.log('sucess Save')
                $state.go('backoffice.admin.listBanks')
            }, function(errorResponse) {
                console.log(errorResponse)
                //$scope.notify('fa fa-lg fa-exclamation-triangle','danger','warning','Erreur lors de la mise a jours du player','body');
            });
        }
        $scope.updateBank = function()
        {
            $scope.bank.$update(function(res) {
                //$scope.find();
                $state.go('backoffice.admin.listBanks');
            }, function(errorResponse) {
                console.log(errorResponse)
                //$scope.notify('fa fa-lg fa-exclamation-triangle','danger','warning','Erreur lors de la mise a jours du player','body');
            });
        }


        function removeBanks(std)
        {
            std.$remove(function(res) {
                $scope.find()
            }, function(errorResponse) {
                console.log(errorResponse)
                //$scope.notify('fa fa-lg fa-exclamation-triangle','danger','warning','Erreur lors de la mise a jours du player','body');
            });
        }


        $scope.confirmAction = function(type,item)
        {
            if(type=='remove')
            {
                alertify.confirm('حذف بنك','هل أنت متأكد من رغبتك في حذف البنك ؟',
                  function(){
                    removeBanks(item)
                },
                  function(){
                })
            }
        }








   ///////////////////7     
    }
]);
