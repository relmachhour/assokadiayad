'use strict';

// Setting up route
angular.module('funds').config(['$stateProvider',
	function($stateProvider) {
		// Articles state routing
		$stateProvider.
		state('backoffice.admin.listFunds', {
			url: '/funds',
			views:{
				"content@backoffice": {
					templateUrl: 'modules/funds/views/list-funds.view.html',
					controller: 'FundsController'
				}
			},
			security:{
				requiredAuth: true,
				requiredPermissions:  ['ROLE_SUPER_ROOT','ROLE_SUPER_ADMIN','ROLE_ADMIN','ROLE_USER']
			}	
		}).
		state('backoffice.admin.createFunds', {
			url: '/funds/create',
			views:{
				"content@backoffice": {
					templateUrl: 'modules/funds/views/create-fund.view.html',
					controller: 'FundsController'
				}
			},
			security:{
				requiredAuth: true,
				requiredPermissions:  ['ROLE_SUPER_ROOT','ROLE_SUPER_ADMIN','ROLE_ADMIN','ROLE_USER']
			}	
		}).
		state('backoffice.admin.editFunds', {
			url: '/funds/:fundId/edit',
			views:{
				"content@backoffice": {
					templateUrl: 'modules/funds/views/edit-fund.view.html',
					controller: 'FundsController'
				}
			},
			security:{
				requiredAuth: true,
				requiredPermissions:  ['ROLE_SUPER_ROOT','ROLE_SUPER_ADMIN','ROLE_ADMIN','ROLE_USER']
			}	
		})
		;
	}
]);
