
'use strict';


angular.module('funds').controller('FundsController', ['$scope', '$rootScope', '$http','$state' ,'$stateParams', '$location', 'Authentication','$uibModal','Funds',
    function($scope, $rootScope, $http,$state, $stateParams, $location, Authentication,$uibModal,Funds) {

        $scope.newFund = {};

        $scope.find = function()
        {
            Funds.query().$promise.then(function(result) {
                $scope.funds = result;
            }); 
        }
        $scope.find()
        
        $scope.findOne = function()
        {
            Funds.get({
                fundId: $stateParams.fundId
            }).$promise.then(function(result) {
                    $scope.fund = result
            }, function(error) {
                console.log(error)
            })
        }



        $scope.saveNewFund = function()
        {
            var saveFund = new Funds($scope.newFund) 

            saveFund.$save(function(res) {
                console.log('sucess Save')
                $state.go('backoffice.admin.listFunds')
            }, function(errorResponse) {
                console.log(errorResponse)
                //$scope.notify('fa fa-lg fa-exclamation-triangle','danger','warning','Erreur lors de la mise a jours du player','body');
            });
        }
        $scope.updateFund = function()
        {
            $scope.fund.$update(function(res) {
                //$scope.find();
                $state.go('backoffice.admin.listFunds');
            }, function(errorResponse) {
                console.log(errorResponse)
                //$scope.notify('fa fa-lg fa-exclamation-triangle','danger','warning','Erreur lors de la mise a jours du player','body');
            });
        }


        function removeFunds(std)
        {
            std.$remove(function(res) {
                $scope.find()
            }, function(errorResponse) {
                console.log(errorResponse)
                //$scope.notify('fa fa-lg fa-exclamation-triangle','danger','warning','Erreur lors de la mise a jours du player','body');
            });
        }


        $scope.confirmAction = function(type,item)
        {
            if(type=='remove')
            {
                alertify.confirm('حذف صندوق المصاريف','هل أنت متأكد من رغبتك في حذف صندوق المصاريف ؟',
                  function(){
                    removeFunds(item)
                },
                  function(){
                })
            }
        }








   ///////////////////7     
    }
]);
