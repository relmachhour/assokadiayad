'use strict';

angular.module('admins').factory('Admins', ['$resource',
	function($resource){
		return $resource('/api/admins/:adminId',{
	      adminId: '@id'
	    },{
	      update: { method: 'PUT'}
	    });
	}
]);
