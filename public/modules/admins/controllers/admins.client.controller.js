'use strict';

angular.module('admins').controller('AdminController', ['$scope', '$stateParams','$state', '$location', 'Socket','Authentication', '$http','Admins',
	function($scope, $stateParams,$state, $location, Socket,Authentication,$http, Admins) {


	// Find a list of Admins
	$scope.find = function() {
		Admins.query().$promise.then(function(adm){
			$scope.admins = adm;
		})
	};


	$scope.findOne = function()
    {
        Admins.get({
                adminId: $stateParams.adminId
        }).$promise.then(function(result) {
                $scope.admin = result               
        }, function(error) {
            console.log(error)
            $scope.notify('fa fa-lg fa-exclamation-triangle','danger','Erreur',"Can't Send the request",'body');
        })
    }

	// Remove existing Device
	function removeUser(admin) {
		if (admin) {
			admin.$remove(function(result){
				$scope.find()
			},function(err){
				$scope.notify('fa fa-lg fa-exclamation-triangle','danger','Erreur',"Can't Send the request",'body');
			});

		}
	};

	$scope.switchStatus= function(admin){
		$http.post('/api/admins/'+admin.id+'/switch-state').success(
			function(res){
				$scope.find()
			}
		);
	};



	
	$scope.saveNewUser = function() 
	{
		console.log('xxxx')
		if(!$scope.newAdmin || $scope.newAdmin.firstName ==undefined || $scope.newAdmin.firstName=='' || $scope.newAdmin.username == undefined || $scope.newAdmin.username=='' || $scope.newAdmin.email ==undefined || $scope.newAdmin.email=='' || $scope.newAdmin.password==undefined ||$scope.newAdmin.password=='')
		{
			$scope.notify('fa fa-lg fa-exclamation-triangle','danger','Erreur','All fields must be completed','body');
		}
		else
		{
			var adms = new Admins({
			firstName: $scope.newAdmin.firstName,
			lastName: $scope.newAdmin.lastName,
			username: $scope.newAdmin.username,
			email: $scope.newAdmin.email,
			password: $scope.newAdmin.password,
			roles: $scope.newAdmin.roles
			});
			adms.$save(function(response) {
				console.log('OK SAVE')
				$state.go('backoffice.admin.listAdminsData')
			}, function(errorResponse) {
				$scope.notify('fa fa-lg fa-exclamation-triangle','danger','Erreur','Erreur lors de la création du Message RSS','body');
			});
		}
	};



	$scope.updateUser = function(data) 
	{
		if(!$scope.admin || $scope.admin.firstName ==undefined || $scope.admin.firstName=='' || $scope.admin.username == undefined || $scope.admin.username=='' || $scope.admin.email ==undefined || $scope.admin.email=='')
		{
			$scope.notify('fa fa-lg fa-exclamation-triangle','danger','Erreur','All fields must be completed','body');
		}
		else{
				$scope.admin.$update(function(){
					$state.go('backoffice.admin.listAdminsData')
				}, function(errorResponse) {
					$scope.notify('fa fa-lg fa-exclamation-triangle','danger','Erreur',"Can't Send the request",'body');
				});
			}
	
	};

	$scope.confirmAction = function(type,item)
    {
        if(type=='remove')
        {
            alertify.confirm('Delete User','Do you want to delete User " '+item.displayName+' "',
              function(){
                removeUser(item)
            },
              function(){
            })
        }
    };



	}
]);
