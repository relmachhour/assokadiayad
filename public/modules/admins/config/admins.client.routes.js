'use strict';

//Setting up route
angular.module('admins').config(['$stateProvider',
	function($stateProvider) {
		// admin state routing
		$stateProvider.
		state('backoffice.admin.listAdminsData', {
			url: '/users',
			views:{
				"content@backoffice": {
					templateUrl: 'modules/admins/views/list-admins.client.view.html',
					resolve: {
						admin: function(){
							return {firstname:'', lastname:'', email:'', roles:[], locked: false, created: Date.now()};
						}
					},
					controller: 'AdminController'
				}
			},
			security:{
				requiredAuth: true,
				requiredPermissions: ['ROLE_SUPER_ADMIN','ROLE_ADMIN']
			}
		}).
		state('backoffice.admin.createAdmin', {
			url: '/users/create',
			views:{
				"content@backoffice": {
					templateUrl: 'modules/admins/views/create-admin.client.view.html',
					resolve: {
						admin: function(){
							return {firstname:'', lastname:'', email:'', roles:['ROLE_SUPER_ADMIN','ROLE_ADMIN'], locked: false, password:''};
						}
					},
					controller: 'AdminController'
				}
			},
			security:{
				requiredAuth: true,
				requiredPermissions: ['ROLE_SUPER_ADMIN','ROLE_ADMIN']
			}

		}).
		state('backoffice.admin.editAdmin',{
			url: '/users/:adminId/edit',
			views:{
				"content@backoffice": {
					templateUrl: 'modules/admins/views/edit-admin.client.view.html',
					resolve:{
						admin: function($stateParams,Admins){
							return Admins.get({
								adminId: $stateParams.adminId
								}).$promise.then(function(result){
									return result;
								});
							}
						},
					controller: 'AdminController'
				}
			},
			security:{
				requiredAuth: true,
				requiredPermissions: ['ROLE_SUPER_ADMIN','ROLE_ADMIN']
			}

		});

	}
]);
