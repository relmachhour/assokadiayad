'use strict';

// Setting up route
angular.module('students').config(['$stateProvider',
	function($stateProvider) {
		// Articles state routing
		$stateProvider.
		state('backoffice.admin.listStudents', {
			url: '/students',
			views:{
				"content@backoffice": {
					templateUrl: 'modules/students/views/list-students.view.html',
					controller: 'StudentsController'
				}
			},
			security:{
				requiredAuth: true,
				requiredPermissions:  ['ROLE_SUPER_ROOT','ROLE_SUPER_ADMIN','ROLE_ADMIN','ROLE_USER']
			}	
		}).
		state('backoffice.admin.createStudents', {
			url: '/students/create',
			views:{
				"content@backoffice": {
					templateUrl: 'modules/students/views/create-student.view.html',
					controller: 'StudentsController'
				}
			},
			security:{
				requiredAuth: true,
				requiredPermissions:  ['ROLE_SUPER_ROOT','ROLE_SUPER_ADMIN','ROLE_ADMIN','ROLE_USER']
			}	
		}).
		state('backoffice.admin.editStudents', {
			url: '/students/:studentId/edit',
			views:{
				"content@backoffice": {
					templateUrl: 'modules/students/views/edit-student.view.html',
					controller: 'StudentsController'
				}
			},
			security:{
				requiredAuth: true,
				requiredPermissions:  ['ROLE_SUPER_ROOT','ROLE_SUPER_ADMIN','ROLE_ADMIN','ROLE_USER']
			}	
		})
		;
	}
]);
