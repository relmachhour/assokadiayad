
'use strict';


angular.module('students').controller('StudentsController', ['$scope', '$rootScope', '$http','$state' ,'$stateParams', '$location', 'Authentication','$uibModal','Students','Classes',
    function($scope, $rootScope, $http,$state, $stateParams, $location, Authentication,$uibModal,Students,Classes) {

        $scope.newStudent = {};
        $scope.selectedClasses= [];

        $scope.find = function()
        {
            console.log('in find')
            Students.query().$promise.then(function(result) {
                $scope.students = result;
                console.log(result)
            }); 
        }
        $scope.find()
        $scope.findCreate = function()
        {
            Classes.query().$promise.then(function(result) {
                $scope.classes = result;
                console.log(result)
                findClassesId()
            }); 
        }
        
        $scope.findOne = function()
        {
            Students.get({
                studentId: $stateParams.studentId
            }).$promise.then(function(result) {
                    $scope.student = result
                    $scope.student.birthDate = new Date($scope.student.birthDate)
                    console.log($scope.student)
            }, function(error) {
                console.log(error)
            })
        }



        $scope.saveNewStudent = function()
        {
            var saveStudent = new Students($scope.newStudent) 
                saveStudent.classes = $scope.selectedClasses

            saveStudent.$save(function(res) {
                console.log('sucess Save')
                $state.go('backoffice.admin.listStudents')
            }, function(errorResponse) {
                console.log(errorResponse)
                //$scope.notify('fa fa-lg fa-exclamation-triangle','danger','warning','Erreur lors de la mise a jours du player','body');
            });
        }
        $scope.updateStudent = function()
        {
            $scope.student.newClasses = $scope.selectedClasses
            $scope.student.$update(function(res) {
                //$scope.find();
                $state.go('backoffice.admin.listStudents');
            }, function(errorResponse) {
                console.log(errorResponse)
                //$scope.notify('fa fa-lg fa-exclamation-triangle','danger','warning','Erreur lors de la mise a jours du player','body');
            });
        }


        function removeStudents(std)
        {
            std.$remove(function(res) {
                $scope.find()
            }, function(errorResponse) {
                console.log(errorResponse)
                //$scope.notify('fa fa-lg fa-exclamation-triangle','danger','warning','Erreur lors de la mise a jours du player','body');
            });
        }


        $scope.confirmAction = function(type,item)
        {
            if(type=='remove')
            {
                alertify.confirm('حذف تلميذ','هل أنت متأكد من رغبتك في حذف التلميذ ؟',
                  function(){
                    removeStudents(item)
                },
                  function(){
                })
            }
        }




        $scope.checkSelectedClass = function(id)
        {
            console.log('access to check')
            console.log($scope.selectedClasses)
            var idx = $scope.selectedClasses.indexOf(id);
            console.log('idx ',idx)
            if(idx>-1){
                console.log('remove already ID: ',id)
                $scope.selectedClasses.splice(idx, 1)
            }else{
                console.log('pus new ID: ',id)
                $scope.selectedClasses.push(id)
            }
            console.log($scope.selectedClasses)
        }




        function findClassesId()
        {
            $scope.student.classes.forEach(function(cls){
                $scope.selectedClasses.push(cls.id)
            })
        }















   ///////////////////7     
    }
]);
