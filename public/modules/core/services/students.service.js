'use strict';

//Articles service used for communicating with the students REST endpoints
angular.module('core').factory('Students', ['$resource',
	function($resource) {
		return $resource('students/:studentId', {
			studentId: '@id'
		}, {
			update: {
				method: 'PUT'
			}
		});
	}
]);