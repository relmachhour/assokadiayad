'use strict';

//Articles service used for communicating with the type_incomes REST endpoints
angular.module('core').factory('Type_incomes', ['$resource',
	function($resource) {
		return $resource('type_incomes/:type_incomeId', {
			type_incomeId: '@id'
		}, {
			update: {
				method: 'PUT'
			}
		});
	}
]);