'use strict';

//Articles service used for communicating with the expenses REST endpoints
angular.module('core').factory('Expenses', ['$resource',
	function($resource) {
		return $resource('expenses/:expenseId', {
			expenseId: '@id'
		}, {
			update: {
				method: 'PUT'
			}
		});
	}
]);