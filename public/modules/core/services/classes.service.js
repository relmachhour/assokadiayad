'use strict';

//Articles service used for communicating with the classes REST endpoints
angular.module('core').factory('Classes', ['$resource',
	function($resource) {
		return $resource('classes/:classId', {
			classId: '@id'
		}, {
			update: {
				method: 'PUT'
			}
		});
	}
]);