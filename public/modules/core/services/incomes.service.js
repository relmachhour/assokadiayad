'use strict';

//Articles service used for communicating with the incomes REST endpoints
angular.module('core').factory('Incomes', ['$resource',
	function($resource) {
		return $resource('incomes/:incomeId', {
			incomeId: '@id'
		}, {
			update: {
				method: 'PUT'
			}
		});
	}
]);