'use strict';

//Articles service used for communicating with the type_expenses REST endpoints
angular.module('core').factory('Type_expenses', ['$resource',
	function($resource) {
		return $resource('type_expenses/:type_expenseId', {
			type_expenseId: '@id'
		}, {
			update: {
				method: 'PUT'
			}
		});
	}
]);