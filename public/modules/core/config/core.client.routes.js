'use strict';

// Setting up route
angular.module('core').config(['$stateProvider', '$urlRouterProvider','$locationProvider',
	function($stateProvider, $urlRouterProvider,$locationProvider) {
		// Redirect to home view when route not found
		//$locationProvider.hashPrefix('');


		$urlRouterProvider.otherwise('backoffice.admin.dashboard');
		

		// Home state routing
		$stateProvider
		.state('backoffice', {
	        //url: '',
	        abstract:true,
	        templateUrl: 'modules/core/views/layout.client.view.html',
			controller: 'MasterController'
	    })
	    .state('backoffice.admin', {
	        url: '',
	        views:{
				'header': {
					templateUrl: 'modules/core/views/header.client.view.html',
					controller: 'HeaderController'
				},
				'sidebar_right': {
					templateUrl: 'modules/core/views/sidebar_right.client.view.html',
					controller: 'SidebarRightController'
					
				}
			}
	    })
		.state('unauthorized', {
			url: '/unauthorized',
			templateUrl: 'modules/core/views/unauthorized.client.view.html'
		})
		.state('error400', {
			url: '/bad-request',
			templateUrl: 'modules/core/views/400.client.view.html'
		})
		.state('error500', {
			url: '/server-error',
			templateUrl: 'modules/core/views/500.client.view.html'
		});
	}
]);