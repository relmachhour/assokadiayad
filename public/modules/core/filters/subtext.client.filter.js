'use strict';

angular.module('core').filter('subtext', [
	function() {
		return function(input) {
			// Subtext directive logic
			// ...

			return input.substring(0,22);
		};
	}
]);
