'use strict';

angular.module('core').filter('number', [
	function() {
		return function(input) {
			
			return Math.round(parseFloat(input));
		};
	}
]);
