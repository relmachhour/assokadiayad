'use strict';


angular.module('core').directive('floatOnly', function() {
  return {
    require: 'ngModel',
    restrict: 'A',
    link: function(scope, element, attrs, modelCtrl) {
      modelCtrl.$parsers.push(function(inputValue) {
        if (inputValue === undefined) return '';
        var cleanInputValue;
        // Remove forbidden characters
        cleanInputValue = inputValue.replace(',', '.') // change commas to dots
          .replace(/[^\d.]/g, '') // keep numbers and dots
          .replace(/\./, "x") // change the first dot in X
          .replace(/\./g, "") // remove all dots
          .replace(/x/, "."); // change X to dot
        if (cleanInputValue != inputValue) {
          modelCtrl.$setViewValue(cleanInputValue);
          modelCtrl.$render();
        }
        return parseFloat(cleanInputValue);
      });
    }
  }
});

angular.module('core').controller('MasterController', ['$scope', '$sce', '$http', '$state', '$rootScope', 'Socket',
    function($scope, $sce, $http, $state, $rootScope, Socket) {
        
       
     $scope.initStyles = function()
     {
        $scope.heightRightBar = document.getElementById('sidebar_right').offsetHeight;

        console.log($scope.heightRightBar)
         if($scope.heightRightBar!=0){
            $scope.contentHeight = ($scope.heightRightBar)+80+'px'
          }else{
            $scope.contentHeight = 'auto'//'102vh'
          }
        
            $scope.$apply()
          
     }

   

    $(document).ready(function(){      
           $scope.initStyles()
    }); 


    $scope.signOutAll = function($event) {
        localStorage.removeItem('token');
        localStorage.removeItem('roles');
        localStorage.removeItem('name');
       $window.location.reload();
    }


        $scope.checkActive = function(item)
        {
          if($state.current.name==item){
            return true;
          }else{
            return false
          }
          
        }

        window.onresize = function(event) {
           $scope.initStyles()
        };

        $scope.f11 = function()
        {
            console.log('full screan')
        if ((document.fullScreenElement && document.fullScreenElement !== null) ||    
           (!document.mozFullScreen && !document.webkitIsFullScreen)) {
            if (document.documentElement.requestFullScreen) {  
              document.documentElement.requestFullScreen();  
            } else if (document.documentElement.mozRequestFullScreen) {  
              document.documentElement.mozRequestFullScreen();  
            } else if (document.documentElement.webkitRequestFullScreen) {  
              document.documentElement.webkitRequestFullScreen(Element.ALLOW_KEYBOARD_INPUT);  
            }  
          } else {  
            if (document.cancelFullScreen) {  
              document.cancelFullScreen();  
            } else if (document.mozCancelFullScreen) {  
              document.mozCancelFullScreen();  
            } else if (document.webkitCancelFullScreen) {  
              document.webkitCancelFullScreen();  
            }  
          }  

        }

      $scope.captureEcran = function()
      {
            


            html2canvas(document.querySelector("#right_col")).then(canvas => {
                document.body.appendChild(canvas)
                saveAs(canvas.toDataURL(), 'capture.png');
            });

            function saveAs(uri, filename) {

                var link = document.createElement('a');

                if (typeof link.download === 'string') {

                    link.href = uri;
                    link.download = filename;

                    //Firefox requires the link to be in the body
                    document.body.appendChild(link);

                    //simulate click
                    link.click();

                    //remove the link when done
                    document.body.removeChild(link);

                } else {

                    window.open(uri);
                }
            }

      }





    }
]);
