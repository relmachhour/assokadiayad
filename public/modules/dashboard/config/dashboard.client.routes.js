'use strict';

// Setting up route
angular.module('dashboard').config(['$stateProvider',
	function($stateProvider) {
		// Articles state routing
		$stateProvider.
		state('backoffice.admin.dashboard', {
			url: '/dashboard',
			views:{
				"content@backoffice": {
					templateUrl: 'modules/dashboard/views/dashboard.client.view.html',
					controller: 'DashboardController'
				}
			},
			security:{
				requiredAuth: true,
				requiredPermissions: ['ROLE_SUPER_ROOT','ROLE_SUPER_ADMIN','ROLE_ADMIN','ROLE_USER']
			}
		}).
		state('backoffice.admin.history', {
			url: '/history',
			views:{
				"content@backoffice": {
					templateUrl: 'modules/dashboard/views/history.client.view.html',
					controller: 'HistoryController'
				}
			},
			security:{
				requiredAuth: true,
				requiredPermissions: ['ROLE_SUPER_ROOT','ROLE_SUPER_ADMIN','ROLE_ADMIN','ROLE_USER']
			}
		}).
		state('backoffice.admin.historyStudentParticipation', {
			url: '/student-participation',
			views:{
				"content@backoffice": {
					templateUrl: 'modules/dashboard/views/student-participation.client.view.html',
					controller: 'HistoryController'
				}
			},
			security:{
				requiredAuth: true,
				requiredPermissions: ['ROLE_SUPER_ROOT','ROLE_SUPER_ADMIN','ROLE_ADMIN','ROLE_USER']
			}
		}).
		
		state('backoffice.admin.tableFunds', {
			url: '/funds-table',
			views:{
				"content@backoffice": {
					templateUrl: 'modules/dashboard/views/funds-table.client.view.html',
					controller: 'HistoryController'
				}
			},
			security:{
				requiredAuth: true,
				requiredPermissions: ['ROLE_SUPER_ROOT','ROLE_SUPER_ADMIN','ROLE_ADMIN','ROLE_USER']
			}
		}).
		state('backoffice.admin.tableBank', {
			url: '/banks-table',
			views:{
				"content@backoffice": {
					templateUrl: 'modules/dashboard/views/banks-table.client.view.html',
					controller: 'HistoryController'
				}
			},
			security:{
				requiredAuth: true,
				requiredPermissions: ['ROLE_SUPER_ROOT','ROLE_SUPER_ADMIN','ROLE_ADMIN','ROLE_USER']
			}
		}).
		state('backoffice.admin.tableIncomes', {
			url: '/incomes-table',
			views:{
				"content@backoffice": {
					templateUrl: 'modules/dashboard/views/incomes-table.client.view.html',
					controller: 'HistoryController'
				}
			},
			security:{
				requiredAuth: true,
				requiredPermissions: ['ROLE_SUPER_ROOT','ROLE_SUPER_ADMIN','ROLE_ADMIN','ROLE_USER']
			}
		}).
		state('backoffice.admin.tableExpenses', {
			url: '/expenses-table',
			views:{
				"content@backoffice": {
					templateUrl: 'modules/dashboard/views/expenses-table.client.view.html',
					controller: 'HistoryController'
				}
			},
			security:{
				requiredAuth: true,
				requiredPermissions: ['ROLE_SUPER_ROOT','ROLE_SUPER_ADMIN','ROLE_ADMIN','ROLE_USER']
			}
		})
		
	}
]);
