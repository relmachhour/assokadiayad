'use strict';


angular.module('dashboard').controller('DashboardController', ['$scope', '$rootScope', '$http', '$stateParams', '$location', 'Authentication',
    function($scope, $rootScope, $http, $stateParams, $location, Authentication) {

        $scope.authentication = Authentication;

        $scope.colors = [
          {
            backgroundColor: 'rgb(69, 242, 69)',
            pointBackgroundColor: 'rgb(69, 242, 69)',
            pointHoverBackgroundColor: 'rgb(69, 242, 69)',
            borderColor: 'rgb(69, 242, 69)',
            pointBorderColor: 'rgb(69, 242, 69)',
            pointHoverBorderColor: 'rgb(69, 242, 69)'
          },{
            backgroundColor: 'rgba(0,255,255)',
            pointBackgroundColor: 'rgba(0,255,255)',
            pointHoverBackgroundColor: 'rgba(0,255,255)',
            borderColor: 'rgba(0,255,255)',
            pointBorderColor: 'rgba(0,255,255)',
            pointHoverBorderColor: 'rgba(0,255,255)'
          }, {
            backgroundColor: 'rgba(0,0,255)',
            pointBackgroundColor: 'rgba(0,0,255)',
            pointHoverBackgroundColor: 'rgba(0,0,255)',
            borderColor: 'rgba(0,0,255)',
            pointBorderColor: 'rgba(0,0,255)',
            pointHoverBorderColor: 'rgba(0,0,255)'
          },{ 
            backgroundColor: 'rgba(255,0,0)',
            pointBackgroundColor: 'rgba(255,0,0)',
            pointHoverBackgroundColor: 'rgba(255,0,0)',
            borderColor: 'rgba(255,0,0)',
            pointBorderColor: 'rgba(255,0,0)',
            pointHoverBorderColor: 'rgba(255,0,0)'
          }, {
            backgroundColor: 'rgba(255,0,255)',
            pointBackgroundColor: 'rgba(255,0,255)',
            pointHoverBackgroundColor: 'rgba(255,0,255)',
            borderColor: 'rgba(255,0,255)',
            pointBorderColor: 'rgba(255,0,255)',
            pointHoverBorderColor: 'rgba(255,0,255)'
          }
        ]
     
        Chart.defaults.global.colors = $scope.colors
        $scope.optionLine = {
            responsive: true, 
            maintainAspectRatio: false,
            legend: {display: true}     
        }




            $scope.initData = function()
            {
                $http({
                  method: 'GET',
                  url: '/api/dashboard/report'
                }).then(function successCallback(response) {
                    $scope.PieE = response.data.pieE[0]
                    $scope.PieI = response.data.pieI[0]
                    $scope.kpi = response.data.kpi[0]
                    $scope.pieP = response.data.pieP[0]
                    $scope.pieStud = {};
                    $scope.pieStud.name = ['محصل عليه','غير محصل عليه'] 
                    $scope.pieStud.data = [$scope.pieP.student_payement,$scope.pieP.total_student-$scope.pieP.student_payement] 
                    $scope.listStudent = response.data.listStudent
                    initGraph(response.data.graphs[0])
                }, function errorCallback(response) {
                        console.log(response)
                });
            
            }   
            $scope.initData()
		
        
            function initGraph(item)
            {
                $scope.graphData = {'labels':item.months,'series':['مصاريف','مداخيل'],'data':[item.expenses,item.incomes]}
            }









    }
]);
