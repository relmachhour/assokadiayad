'use strict';


angular.module('dashboard').controller('HistoryController', ['$scope', '$rootScope', '$http', '$stateParams', '$location', 'Authentication','Students',
    function($scope, $rootScope, $http, $stateParams, $location, Authentication,Students) {

        $scope.authentication = Authentication;
        $scope.filter = {from:createDateAsUTC('from'),to:createDateAsUTC('to')}
       

        $scope.findStudent = function()
        {
            Students.query().$promise.then(function(result) {
                $scope.students = result;
            });
        }
        $scope.searchDataHistory = function()
        {
            $http({
              method: 'POST',
              url: '/api/hisotry/find',
              data:$scope.filter
            }).then(function successCallback(response) {
                $scope.history = response.data
            }, function errorCallback(response) {
                    console.log(response)
            });
            
        }
        $scope.searchFundsTable = function()
        {
            $http({
              method: 'POST',
              url: '/api/hisotry/funds-table',
              data:$scope.filter
            }).then(function successCallback(response) {
                $scope.history = response.data
            }, function errorCallback(response) {
                    console.log(response)
            });
            
        }
        $scope.searchBanksTable = function()
        {
            $http({
              method: 'POST',
              url: '/api/hisotry/banks-table',
              data:$scope.filter
            }).then(function successCallback(response) {
                $scope.history = response.data
            }, function errorCallback(response) {
                    console.log(response)
            });
            
        }
        $scope.searchIncomesTable = function()
        {
            $http({
              method: 'POST',
              url: '/api/hisotry/incomes-table',
              data:$scope.filter
            }).then(function successCallback(response) {
                $scope.history = response.data
            }, function errorCallback(response) {
                    console.log(response)
            });
            
        }
        $scope.searchExpensesTable = function()
        {
            $http({
              method: 'POST',
              url: '/api/hisotry/expenses-table',
              data:$scope.filter
            }).then(function successCallback(response) {
                $scope.history = response.data
            }, function errorCallback(response) {
                    console.log(response)
            });
            
        }

        $scope.downData = function()
        {
            $http({
              method: 'POST',
              url: '/api/hisotry/export',
              data:$scope.filter
            }).then(function successCallback(result) {
                $http.get(result.data.url).then(function successCallback(response) {
                  var a = document.createElement("a");
                  document.body.appendChild(a);
                  var file = new Blob([response.data], {type: 'application/csv'});
                  var fileURL = window.URL.createObjectURL(file);
                  a.href = fileURL;
                  a.download = 'history.csv';
                  a.click();
                  $scope.waitModalex=false
                  $scope.waitModal = false
              })
            }, function errorCallback(response) {
                    console.log(response)
            });
            
        }

        $scope.searchStudentParticipation = function()
        {
          $http({
              method: 'POST',
              url: '/api/hisotry/student-participation',
              data:$scope.filter
            }).then(function successCallback(response) {
                $scope.participations = response.data
            }, function errorCallback(response) {
                    console.log(response)
            });
        }
         $scope.downStudentParticipation = function()
        {
          
        }





        $scope.getMonth = function(date,index)
        {
            var index;
            if(date){
              index = new Date(date).getMonth()
            }else{
              index = index-1
            }
            let month = ['يناير','فبراير','مارس','أبريل','ماي','يونيو','يوليوز','غشت','شتنبر','أكتوبر','نونبر','دجنبر']
            return month[index]
        }



        function createDateAsUTC(type) {
          var date = new Date() 
            if(type=='to') return new Date(date.getFullYear(), date.getMonth(), date.getDate(), date.getHours(), date.getMinutes());
            if(type=='from')return new Date(date.getFullYear(), date.getMonth(), date.getDate(), date.getHours()-24, date.getMinutes());
        }







    }
]);
