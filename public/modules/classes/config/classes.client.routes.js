'use strict';

// Setting up route
angular.module('classes').config(['$stateProvider',
	function($stateProvider) {
		// Articles state routing
		$stateProvider.
		state('backoffice.admin.listClasses', {
			url: '/classes',
			views:{
				"content@backoffice": {
					templateUrl: 'modules/classes/views/list-classes.view.html',
					controller: 'ClassesController'
				}
			},
			security:{
				requiredAuth: true,
				requiredPermissions:  ['ROLE_SUPER_ROOT','ROLE_SUPER_ADMIN','ROLE_ADMIN','ROLE_USER']
			}	
		}).
		state('backoffice.admin.listeachClasses', {
			url: '/classes',
			views:{
				"content@backoffice": {
					templateUrl: 'modules/classes/views/listeachclass.view.html',
					controller: 'ClassesController'
				}
			},
			security:{
				requiredAuth: true,
				requiredPermissions:  ['ROLE_SUPER_ROOT','ROLE_SUPER_ADMIN','ROLE_ADMIN','ROLE_USER']
			}	
		}).
		state('backoffice.admin.createClasses', {
			url: '/classes/create',
			views:{
				"content@backoffice": {
					templateUrl: 'modules/classes/views/create-class.view.html',
					controller: 'ClassesController'
				}
			},
			security:{
				requiredAuth: true,
				requiredPermissions:  ['ROLE_SUPER_ROOT','ROLE_SUPER_ADMIN','ROLE_ADMIN','ROLE_USER']
			}	
		}).
		state('backoffice.admin.editClasses', {
			url: '/classes/:classId/edit',
			views:{
				"content@backoffice": {
					templateUrl: 'modules/classes/views/edit-class.view.html',
					controller: 'ClassesController'
				}
			},
			security:{
				requiredAuth: true,
				requiredPermissions:  ['ROLE_SUPER_ROOT','ROLE_SUPER_ADMIN','ROLE_ADMIN','ROLE_USER']
			}	
		})
		;
	}
]);
