
'use strict';


var app = angular.module('classes').controller('ClassesController', ['$scope', '$rootScope', '$http','$state','$stateParams', '$location', 'Authentication','$uibModal','Classes',
    function($scope, $rootScope, $http,$state, $stateParams, $location, Authentication,$uibModal,Classes) {

        $scope.newClass = {};
        $scope.selectedClasses= [];
        $scope.studentslist = [];
        $scope.find = function()
        {
            Classes.query().$promise.then(function(result) {
                $scope.classes = result;
                console.log(result[2].students)
            }); 
        }
        $scope.find();

        $scope.findOne = function()
        {
            Classes.get({
                classId: $stateParams.classId
            }).$promise.then(function(result) {
                    $scope.oneClass = result
            }, function(error) {
                console.log(error)
            })
        }

        $scope.searchDataHistory = function()
        {
            /*$http({
              method: 'POST',
              url: '/api/hisotry/find',
              data:$scope.filter
            }).then(function successCallback(response) {
                $scope.history = response.data
            }, function errorCallback(response) {
                    console.log(response)
            });*/
            console.log('searchDataHistory')
            console.log($scope.studentslist);
            $scope.className = $scope.classes[$scope.studentslist-1].name;
            $scope.studentslists = $scope.classes[$scope.studentslist-1].students;
            console.log($scope.studentslists);
            
        }

        $scope.downData = function()
        {
            /*console.log("downData")
            $scope.data=  [{"agence":"CTM","secteur":"Safi","statutImp":"operationnel"}];
            html2canvas(document.getElementById('exportthis'), {
            onrendered: function (canvas) {
                var data = canvas.toDataURL();
                var docDefinition = {
                    content: [{
                        image: data,
                        width: 300,
                    }]
                };
                pdfMake.createPdf(docDefinition).download("test.pdf");
            }
            });*/
        }

        $scope.saveNewClass = function()
        {

            var saveClass = new Classes($scope.newClass) 
            saveClass.$save(function(res) {
                console.log('sucess Save')
                $scope.find()
                $state.go('backoffice.admin.listClasses')
            }, function(errorResponse) {
                console.log(errorResponse)
                //$scope.notify('fa fa-lg fa-exclamation-triangle','danger','warning','Erreur lors de la mise a jours du player','body');
            });
        }
        $scope.updateClass = function()
        {
           
            $scope.oneClass.$update(function(res) {
                $scope.find();
                $state.go('backoffice.admin.listClasses');
            }, function(errorResponse) {
                console.log(errorResponse)
                //$scope.notify('fa fa-lg fa-exclamation-triangle','danger','warning','Erreur lors de la mise a jours du player','body');
            });
        }




        $scope.checkSelectedClass = function(id)
        {
            console.log('access to check')
            console.log($scope.selectedClasses)
            var idx = $scope.selectedClasses.indexOf(id);
            console.log('idx ',idx)
            if(idx>-1){
                console.log('remove already ID: ',id)
                $scope.selectedClasses.splice(idx, 1)
            }else{
                console.log('pus new ID: ',id)
                $scope.selectedClasses.push(id)
            }
            console.log($scope.selectedClasses)
        }



        function removeClasses(cls)
        {
            cls.$remove(function(res) {
                $scope.find()
            }, function(errorResponse) {
                console.log(errorResponse)
                //$scope.notify('fa fa-lg fa-exclamation-triangle','danger','warning','Erreur lors de la mise a jours du player','body');
            });
        }


        $scope.confirmAction = function(type,item)
        {
            if(type=='remove')
            {
                alertify.confirm('حذف قسم','هل أنت متأكد من رغبتك في حذف القسم ؟',
                  function(){
                    removeClasses(item)
                },
                  function(){
                })
            }
        }













   ///////////////////7     
    }
]);


app.directive('exportToCsv',function(){
    return {
        restrict: 'A',
        link: function (scope, element, attrs) {
            var el = element[0];
            element.bind('click', function(e){
                console.log('exportToCsv')
                var table = document.getElementById("tablee");  //e.target.nextElementSibling;
                console.log(table.length)
                var csvString = '';
                for(var i=0; i<table.rows.length;i++){
                    var rowData = table.rows[i].cells;
                    for(var j=0; j<rowData.length;j++){
                        csvString = csvString + rowData[j].innerHTML + ",";
                    }
                    csvString = csvString.substring(0,csvString.length - 1);
                    csvString = csvString + "\n";
                }
                csvString = csvString.substring(0, csvString.length - 1);
                var a = $('<a/>', {
                    style:'display:none',
                    href:'data:application/octet-stream;base64,'+btoa(unescape(encodeURIComponent(csvString))),
                    download:'لائحة قسم '+scope.className+'.csv'
                }).appendTo('body')
                a[0].click()
                a.remove();
            });
        }
    }
    });