'use strict';

// Setting up route
angular.module('expenses').config(['$stateProvider',
	function($stateProvider) {
		// Articles state routing
		$stateProvider.
		state('backoffice.admin.listExpenses', {
			url: '/expenses',
			views:{
				"content@backoffice": {
					templateUrl: 'modules/expenses/views/list-expenses.view.html',
					controller: 'ExpensesController'
				}
			},
			security:{
				requiredAuth: true,
				requiredPermissions:  ['ROLE_SUPER_ROOT','ROLE_SUPER_ADMIN','ROLE_ADMIN','ROLE_USER']
			}	
		}).
		state('backoffice.admin.createExpenses', {
			url: '/expenses/create',
			views:{
				"content@backoffice": {
					templateUrl: 'modules/expenses/views/create-expense.view.html',
					controller: 'ExpensesController'
				}
			},
			security:{
				requiredAuth: true,
				requiredPermissions:  ['ROLE_SUPER_ROOT','ROLE_SUPER_ADMIN','ROLE_ADMIN','ROLE_USER']
			}	
		}).
		state('backoffice.admin.editExpenses', {
			url: '/expenses/:expenseId/edit',
			views:{
				"content@backoffice": {
					templateUrl: 'modules/expenses/views/edit-expense.view.html',
					controller: 'ExpensesController'
				}
			},
			security:{
				requiredAuth: true,
				requiredPermissions:  ['ROLE_SUPER_ROOT','ROLE_SUPER_ADMIN','ROLE_ADMIN','ROLE_USER']
			}	
		})
		;
	}
]);
