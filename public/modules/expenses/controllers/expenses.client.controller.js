
'use strict';


angular.module('expenses').controller('ExpensesController', ['$scope', '$rootScope', '$http','$state' ,'$stateParams', '$location', 'Authentication','$uibModal','Expenses','Type_expenses',
    function($scope, $rootScope, $http,$state, $stateParams, $location, Authentication,$uibModal,Expenses,Type_expenses) {

        $scope.newExpense = {};

        $scope.find = function()
        {
            Expenses.query().$promise.then(function(result) {
                $scope.expenses = result;
            }); 
        }
        $scope.find()
        

        $scope.findCreate = function()
        {
            Type_expenses.query().$promise.then(function(result) {
                $scope.type_expenses = result;
            }); 
        };


        $scope.findOne = function()
        {
            Expenses.get({
                expenseId: $stateParams.expenseId
            }).$promise.then(function(result) {
                    $scope.expense = result;
                    $scope.expense.savedAt = new Date(result.savedAt);
            }, function(error) {
                console.log(error)
            })
        }



        $scope.saveNewExpense = function()
        {
            var saveExpense = new Expenses($scope.newExpense) 

            saveExpense.$save(function(res) {
                console.log('sucess Save')
                $state.go('backoffice.admin.listExpenses')
            }, function(errorResponse) {
                console.log(errorResponse)
                //$scope.notify('fa fa-lg fa-exclamation-triangle','danger','warning','Erreur lors de la mise a jours du player','body');
            });
        }
        $scope.updateExpense = function()
        {
            $scope.expense.$update(function(res) {
                //$scope.find();
                $state.go('backoffice.admin.listExpenses');
            }, function(errorResponse) {
                console.log(errorResponse)
                //$scope.notify('fa fa-lg fa-exclamation-triangle','danger','warning','Erreur lors de la mise a jours du player','body');
            });
        }


        function removeExpenses(std)
        {
            std.$remove(function(res) {
                $scope.find()
            }, function(errorResponse) {
                console.log(errorResponse)
                //$scope.notify('fa fa-lg fa-exclamation-triangle','danger','warning','Erreur lors de la mise a jours du player','body');
            });
        }


        $scope.confirmAction = function(type,item)
        {
            if(type=='remove')
            {
                alertify.confirm('حذف المصاريف','هل أنت متأكد من رغبتك في الحذف ؟',
                  function(){
                    removeExpenses(item)
                },
                  function(){
                })
            }
        }








   ///////////////////7     
    }
]);
