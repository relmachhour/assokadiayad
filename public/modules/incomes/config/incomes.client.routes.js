'use strict';

// Setting up route
angular.module('incomes').config(['$stateProvider',
	function($stateProvider) {
		// Articles state routing
		$stateProvider.
		state('backoffice.admin.listIncomes', {
			url: '/incomes',
			views:{
				"content@backoffice": {
					templateUrl: 'modules/incomes/views/list-incomes.view.html',
					controller: 'IncomesController'
				}
			},
			security:{
				requiredAuth: true,
				requiredPermissions:  ['ROLE_SUPER_ROOT','ROLE_SUPER_ADMIN','ROLE_ADMIN','ROLE_USER']
			}	
		}).
		state('backoffice.admin.createIncomes', {
			url: '/incomes/create',
			views:{
				"content@backoffice": {
					templateUrl: 'modules/incomes/views/create-income.view.html',
					controller: 'IncomesController'
				}
			},
			security:{
				requiredAuth: true,
				requiredPermissions:  ['ROLE_SUPER_ROOT','ROLE_SUPER_ADMIN','ROLE_ADMIN','ROLE_USER']
			}	
		}).
		state('backoffice.admin.editIncomes', {
			url: '/incomes/:incomeId/edit',
			views:{
				"content@backoffice": {
					templateUrl: 'modules/incomes/views/edit-income.view.html',
					controller: 'IncomesController'
				}
			},
			security:{
				requiredAuth: true,
				requiredPermissions:  ['ROLE_SUPER_ROOT','ROLE_SUPER_ADMIN','ROLE_ADMIN','ROLE_USER']
			}	
		})
		;
	}
]);
