
'use strict';


angular.module('incomes').controller('IncomesController', ['$scope', '$rootScope', '$http','$state' ,'$stateParams', '$location', 'Authentication','$uibModal','Incomes','Type_incomes','Students',
    function($scope, $rootScope, $http,$state, $stateParams, $location, Authentication,$uibModal,Incomes,Type_incomes,Students) {

        $scope.newIncome = {};
        $scope.listMonth = ['يناير', 'فبراير', 'مارس', 'أبريل', 'ماي', 'يونيو', 'يوليوز', 'غشت', 'شتنبر', 'أكتوبر', 'نونبر', 'دجنبر'];
        $scope.listYear = _.range(new Date().getFullYear()-7, new Date().getFullYear()+2);
        $scope.find = function()
        {
            Incomes.query().$promise.then(function(result) {
                $scope.incomes = result;
            }); 
        }
        $scope.find()
        

        $scope.findCreate = function()
        {
            Type_incomes.query().$promise.then(function(result) {
                $scope.type_incomes = result;
            }); 
            Students.query().$promise.then(function(result) {
                $scope.students = result;
            });
        };


        $scope.findOne = function()
        {
            Incomes.get({
                incomeId: $stateParams.incomeId
            }).$promise.then(function(result) {
                    $scope.income = result;
                    $scope.income.savedAt = new Date(result.savedAt);
                    $scope.typeIncomeEdit($scope.income.typeIncome) 
            }, function(error) {
                console.log(error)
            })
        }

        $scope.typeIncome = function(item)
        {
            $scope.typeIncomeValue = item
            $scope.newIncome.type_income_id = item.id
        }
        $scope.typeIncomeEdit = function(item)
        {
            $scope.typeIncomeValue = item
            $scope.income.type_income_id = item.id;
            if($scope.typeIncomeValue.fromStudent) $scope.income.studentIncome = {'monthName':$scope.listMonth[new Date($scope.income.participationDate).getMonth()],'year':new Date($scope.income.participationDate).getFullYear()} 
        }

        $scope.saveNewIncome = function()
        {   
            if($scope.typeIncomeValue.fromStudent){
                 $scope.newIncome.studentIncome.month = $scope.listMonth.indexOf($scope.newIncome.studentIncome.monthName)
                 $scope.newIncome.participationDate = new Date($scope.newIncome.studentIncome.year,$scope.newIncome.studentIncome.month,15)
            }
            var saveIncome = new Incomes($scope.newIncome) 
            saveIncome.$save(function(res) {
                console.log('sucess Save')
                $state.go('backoffice.admin.listIncomes')
            }, function(errorResponse) {
                console.log(errorResponse)
                //$scope.notify('fa fa-lg fa-exclamation-triangle','danger','warning','Erreur lors de la mise a jours du player','body');
            });
        }
        $scope.updateIncome = function()
        {
            if($scope.income.student) $scope.income.student_id = $scope.income.student.id
            if($scope.typeIncomeValue.fromStudent){
                 $scope.income.studentIncome.month = $scope.listMonth.indexOf($scope.income.studentIncome.monthName)
                 $scope.income.participationDate = new Date($scope.income.studentIncome.year,$scope.income.studentIncome.month,15)
            }
            $scope.income.$update(function(res) {
                //$scope.find();
                $state.go('backoffice.admin.listIncomes');
            }, function(errorResponse) {
                console.log(errorResponse)
                //$scope.notify('fa fa-lg fa-exclamation-triangle','danger','warning','Erreur lors de la mise a jours du player','body');
            });
        }


        function removeIncomes(std)
        {
            std.$remove(function(res) {
                $scope.find()
            }, function(errorResponse) {
                console.log(errorResponse)
                //$scope.notify('fa fa-lg fa-exclamation-triangle','danger','warning','Erreur lors de la mise a jours du player','body');
            });
        }


        $scope.confirmAction = function(type,item)
        {
            if(type=='remove')
            {
                alertify.confirm('حذف المداخيل','هل أنت متأكد من رغبتك في الحذف ؟',
                  function(){
                    removeIncomes(item)
                },
                  function(){
                })
            }
        }








   ///////////////////7     
    }
]);
