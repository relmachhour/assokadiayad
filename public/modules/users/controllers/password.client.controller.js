'use strict';

angular.module('users').controller('PasswordController', ['$scope','Authentication', '$stateParams', '$http', '$location','$state',
	function($scope,Authentication, $stateParams, $http, $location, $state) {

		$scope.loading = false;
		$scope.dsablebut = true;

		// Submit forgotten password account id
		$scope.askForPasswordReset = function() {
			$scope.dsablebut = false
			$scope.success = $scope.error = null;
			$scope.loading=true;
			$http.post('/api/security/auth/forgot', $scope.credentials).success(function(response) {
				// Show user success message and clear form
				$scope.credentials = null;
				$scope.success = response.message;
				$scope.loading=false;
				$state.go('signin')


			}).error(function(response) {
				// Show user error message and clear form
				$scope.credentials = null;
				$scope.error = response.message;
				$scope.loading=false;
				$scope.dsablebut = true
			});
		};

		// Change user password
		$scope.resetUserPassword = function() {
			$scope.dsablebut = false
			$scope.success = $scope.error = null;
			$scope.loading=true;
			$http.post('/api/security/auth/reset/' + $stateParams.token, $scope.passwordDetails).success(function(response) {
				// If successful show success message and clear form
				$scope.passwordDetails = null;
				$scope.loading=false;
				// And redirect to the index page
				$scope.success = response.message;
				Authentication.clearCredentials();
				$state.go('signin')
			}).error(function(response) {
				$scope.error = response.message;
				$scope.loading=false;
				$scope.dsablebut = true
			});
		};
	}
]);
