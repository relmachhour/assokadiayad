'use strict';

angular.module('users').controller('AuthenticationController', ['$scope', '$http', '$location', 'Authentication','$window',
	function($scope, $http, $location, Authentication,$window) {
		$scope.authentication = Authentication;

		// If user is signed in then redirect back home

		$scope.authFunction = function() {
				$http({
	              method: 'POST',
	              url: '/api/security/auth/signin',
	              data:$scope.credentials
	            }).then(function successCallback(response) {
                console.log(response.data)
          						localStorage.setItem('token',response.data.token);
          						localStorage.setItem('roles',response.data.roles);
          						localStorage.setItem('name',response.data.name);
                      window.location.href = '/#!/dashboard';

	            }, function errorCallback(response) {
						      $scope.notify('glyphicon glyphicon-exclamation-sign','danger','Error',response.data.message,'body');

	            });
		};



       /*
       *  Begin Notification Config Section        
       */

          $scope.histories = [];

          $scope.notify = function(icon, type, title, message, parent) {
              var colorIcon = '#ffffff';
              if (title == 'réussie')
                  colorIcon = '#ffffff';
              else if (title == 'avertissement')
                  colorIcon = '#ffffff';
              else if (title == 'erreur')
                  colorIcon = '#ffffff';

              var notify = $.notify({
                  // options
                  icon: icon,
                  title: title,
                  message: message,
              }, {
                  // settings
                  element: parent,
                  type: type,
                  allow_dismiss: true,
                  newest_on_top: true,
                  placement: {
                      from: "top",
                      align: "right"
                  },
                  offset: {
                      x: 10,
                      y: 45
                  },
                  spacing: 0,
                  z_index: 9999,
                  delay: 1500,
                  timer: 500,
                  animate: {
                      enter: 'animated fadeInDown',
                      exit: 'animated fadeOutUp'
                  },
                  icon_type: 'class',
                  template: '<div class="bootstrap-notify" data-notify="container" style="width:250px"><div class="alert alert-{0}" role="alert" style="padding:5px 3px 10px 10px">' +
                      '<a class="btn-close pull-right" aria-hidden="true" data-notify="dismiss"><i class="i-close"></i></a>' +
                      '<span data-notify="icon"  class=""  style="margin-right:10px;color:' + colorIcon + '"></span><span data-notify="title" style="color: #000000; font-weight: 900; font-size: 14pt;font-family: estrangelo_edessaregular,arial;">{1}</span> ' +
                      '<div data-notify="message" style="display: block;font-size:11px;padding:5px 25px 0px 10px">{2}</div>' +
                      '</div></div>'
              });

              $scope.histories.push({
                  type: type,
                  date: new Date(),
                  text: message
              });

          }

          //$scope.notify('glyphicon glyphicon-exclamation-sign','danger','Erreur','il fautt Préciser la nature de la Commande','body');
          //$scope.notify('glyphicon glyphicon-ok-sign', 'info', 'réussie', 'Nouvelle Layout ressut avav sasjdasjdau', 'body');
                             
          /*
       * END Notification Config Section        
       */















	}
]);
