'use strict';

// Setting up route
angular.module('users').config(['$stateProvider',
	function($stateProvider) {
		// Users state routing
		$stateProvider.
		state('profile', {
			url: '/accounts/settings/profile',
			templateUrl: 'modules/users/views/settings/edit-profile.client.view.html'
		}).
		state('signup', {
			url: '/accounts/signup',
			templateUrl: 'modules/users/views/authentication/signup.client.view.html'
		}).
		state('signin', {
			url: '/accounts/signin',
			templateUrl: 'modules/users/views/authentication/signin.client.view.html',
			controller: 'AuthenticationController'
		}).
		state('forgot', {
			url: '/accounts/password/forgot',
			templateUrl: 'modules/users/views/password/forgot-password.client.view.html',
			controller: 'PasswordController'
		}).
		state('reset-invalid', {
			url: '/accounts/password/reset/invalid',
			templateUrl: 'modules/users/views/password/reset-password-invalid.client.view.html'
		}).
		state('reset-success', {
			url: '/accounts/password/reset/success',
			templateUrl: 'modules/users/views/password/reset-password-success.client.view.html'
		}).
		state('reset', {
			url: '/accounts/password/reset/:token',
			templateUrl: 'modules/users/views/password/reset-password.client.view.html',
			controller: 'PasswordController'
		});
	}
]);
