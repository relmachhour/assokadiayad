'use strict';

// Config HTTP Error Handling
angular.module('users').config(['$httpProvider',
	function($httpProvider) {
		// Set the httpProvider "not authorized" interceptor
		$httpProvider.interceptors.push(['$q', '$location', 'Authentication',
			function($q, $location, Authentication) {
				return {
					responseError: function(rejection) {
						console.log('---------------')
						console.log(rejection)
						console.log('---------------')
						switch (rejection.status) {
							case 401:
								// Deauthenticate the global user
								Authentication.clearCredentials();
								// Redirect to signin page
								$location.path('accounts/signin');
								break;

							case 403:
								// Add unauthorized behaviour
								// Redirect to unauthorized page
								console.log('unauthorized****************')
								Authentication.clearCredentials();
								// Redirect to signin page
								$location.path('accounts/signin');
								//$location.path('unauthorized');
								break;

							/*case 500:
								// Add Server Error behaviour
								// Redirect to 500 page
								$location.path('error500');
								break;*/

						}

						return $q.reject(rejection);
					},
					request: function(config){
						config.headers= config.headers || {};
						if(localStorage.getItem('token'))
							config.headers['Authorization']= 'Token ' + localStorage.getItem('token');
						return config;
					}

				};
			}
		]);
	}
]);
