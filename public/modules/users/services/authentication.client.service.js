'use strict';

// Authentication service for user variables
angular.module('users').factory('Authentication', ['$location','$window',
	function($location,$window) {
		var service = this;
	
		service = {

			signInAPIUrl: '/api/security/auth/signin',

			signOutAPIUrl: '/api/security/auth/signout',

			signInURL: '/accounts/administration/companies',

			defaultBackendSignInURL: '/accounts/administration/dashboard',

			defaultSalersSignInURL: '/accounts/utilisateur/dashboard',

			defaultClientSignInURL: '/accounts/utilisateur/dashboard',

			signOutURL: '/accounts/signin',

			defaultSignOutURL: '/accounts/signin',

			token: localStorage.getItem('token'),
			local_token: localStorage.getItem('local_token'),

			isAuthenticated: function(){
				if(service.getToken())
					return true;
				return false;
			},

			setToken: function(token){
				localStorage.setItem('token',token);
			},

			getToken: function(){
				return localStorage.getItem('token');
			},

			setContext: function(context){
				localStorage.setItem('context',context);
			},

			getContext: function(){
				return localStorage.getItem('context');
			},

			clearCredentials: function(){
				localStorage.removeItem('token');
				localStorage.removeItem('roles');
				localStorage.removeItem('context');
			},

			hasRole: function(role){
				return service.hasRoles([role]);
			},

			hasRoles: function(roles){
				return (_.intersection(service.getRoles(), roles).length > 0);

			},

			setRoles: function(roles){

				localStorage.setItem('roles',roles);
			},

			getRoles: function(){
				return JSON.parse(localStorage.getItem('roles'));
			},

			signIn: function(){

			},

			signOut: function(http){
				http.get(service.signOutAPIUrl)
					.success(function(response){

						service.clearCredentials();
						$location.path(service.defaultSignOutURL);

					})
					.error(function(response){
						alert('Error: '+response.message);
					});
			},

			getHomePage: function(){
				if(service.getContext() === 'back-office')
					return service.defaultBackendSignInURL;
				else if(service.getContext() === 'users')
					return service.defaultSalersSignInURL;
			}

		};

		return service;
	}
]);
