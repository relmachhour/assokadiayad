/**
 * dependencies management
 */
var proc_acr = process.arch;
	console.log('Machine type: =============================> '+proc_acr)
if(proc_acr=='x64'){
	var AbusCreator= require('./x64/Abus');
}else if(proc_acr=='arm'){
	var AbusCreator= require('./arm/Abus');
}else {
	var AbusCreator= require('./x32/Abus');
}



/**
 * @constructor Abus Constructor definition
 * @param station the remote station IP
 * @param port the remote station PORT
 */
var Abus = function(localServiceName,station,port,protocol){
	this.proxy = new AbusCreator.abus(localServiceName);
	this.station= station;
	this.port= port;
	this.protocol = protocol;
	this.localServiceName = localServiceName;
	this.dataPortlet = undefined;
	this.jsonResult = undefined;
	this.registredEvents = {};// {eventName:'',descr:'',returns:''}
	this.registredMethods = {};// {eventName:'',descr:'',returns:''}

	if(this.protocol && this.protocol == 'udp'){
		this.proxy.enable_udp();
	}

	this.proxy.start();
};




/**
 * @property service the last service called
 */
Abus.prototype.service = undefined;

/**
 * @property service the last executed method
 */
Abus.prototype.method = undefined;

/**
 * @property results the last results get it
 */
Abus.prototype.results = undefined;


/**
 * @set station ip address
 * @param {String}
 *
 * @return {undefined}
 */
Abus.prototype.setStation= function(station){
  this.station = station;
};

/**
 * @get station ip address
 *
 * @return {String}
 */
Abus.prototype.getStation= function(){
  return this.station;
};

/**
 * @set port
 * @param {Number}
 * @return {undefined}
 */
Abus.prototype.setPort= function(port){
  this.port = port;
};

/**
 * @get port
 * @return {Number}
 */
Abus.prototype.getPort= function(){
  return this.port;
};

/**
 * @Get proxy
 * @return {Object}
 */
Abus.prototype.getProxy= function(){
  return this.proxy;
};




/**
 * set to current service
 * @param {String}
 */
Abus.prototype.use= function(service){
	if(!service || service === ''){
		console.log('the service cant be empty');
		return ;
	}

  this.service = service;
};

/**
 * set to current service
 * @param {String}
 */
Abus.prototype.getServiceURI= function(){

  if(this.station != undefined && this.station != '' && this.protocol != '')
	 	return this.protocol+'://'+this.station+':'+this.port+'@'+this.service;

	return this.service;
};

/**
 * set to current method
 * @param {String}
 */
Abus.prototype.call= function(method){
	if(!method || method === ''){
		console.log('the method cant be empty!!');
		return ;
	}

  this.method = method;

	if(this.getServiceURI() != undefined && this.getServiceURI() != ''){
		this.dataPortlet = this.proxy.request_method_init(this.getServiceURI(),this.method);
	}else{
		console.log('can\'t call the method while the service is not set');
	}

};

/**
 * set the service and the method want to call
 * @param {String}
 */
Abus.prototype.useAndCall= function(service,method){
  this.use(service);
  this.call(method);
};



/**
 * parse the jsonrpc
 *
 * @param {String} jsonStr
 * @return {Object} parsed Data
 */
Abus.prototype.parse= function(jsonStr){
	//console.log('json : '+JSON.stringify(jsonStr));
	var jsonrpc= JSON.parse(jsonStr);

	if(jsonrpc.result != undefined){
		return jsonrpc.result;
	}

	return undefined;
};

/**
 * add parameter to the invoked method
 * @param {String} attrName
 * @param {Number} value
 * @param {V8::Pointer} dataPortlet
 */
Abus.prototype.appendInt= function(attrName,value,dataPortlet){
	if(dataPortlet != undefined){
    this.proxy.append_int(dataPortlet,attrName,value);
  }else{
    this.proxy.append_int(this.dataPortlet,attrName,value);
  }
};

/**
 * add parameter to the invoked method
 * @param {String} attrName
 * @param {Number} value
 * @param {V8::Pointer} dataPortlet
 */
Abus.prototype.appendDouble= function(attrName,value,dataPortlet){
	if(dataPortlet){
		this.proxy.append_double(dataPortlet,attrName,value);
	}else{
		this.proxy.append_double(this.dataPortlet,attrName,value);
	}
};

/**
 * add parameter to the invoked method
 * @param {String} attrName
 * @param {String} value
 * @param {V8::Pointer} dataPortlet
 */
Abus.prototype.appendString= function(attrName,value,dataPortlet){
	if(dataPortlet){
		this.proxy.append_string(dataPortlet,attrName,value);
	}else{
		this.proxy.append_string(this.dataPortlet,attrName,value);
	}
};

/**
 * add parameter to the invoked method
 * @param {String} attrName
 * @param {String} value
 * @param {V8::Pointer} dataPortlet
 */
Abus.prototype.appendBoolean= function(attrName,value,dataPortlet){
	if(dataPortlet){
		this.proxy.append_bool(dataPortlet,attrName,value);
	}else{
		this.proxy.append_bool(this.dataPortlet,attrName,value);
	}
};

/**
 * dynamic append
 * @param {string} the attribute name
 * @param {mixed} the value
 * @param {String} the type of the value
 * @param {V8::Pointer} the abus side pointer
 *
 */
Abus.prototype.append= function(attrName,value,type,dataPortlet){
	if(type == 'i'){
		this.appendInt(attrName,value,dataPortlet);
	}else if(type == 's'){
		this.appendString(attrName,value,dataPortlet);
	}else if(type == 'b'){
		this.appendBoolean(attrName,value,dataPortlet);
	}else if(type == 'd'){
		this.appendDouble(attrName,value,dataPortlet);
	}
};

/**
 * invoke the abus method Synchronous
 * @param
 *
 */
Abus.prototype.invoke= function(){
	this.jsonResult = this.proxy.invoke(this.dataPortlet,0,10);
	
	return this.jsonResult;
}


/**
 * invoke the abus method Asynchronous
 * @param
 *
 */

Abus.prototype.invokeAsync= function(abs,variable,params,callback){
	
	this.proxy.invoke_async(this.dataPortlet ,0,1000,params,function(s,data){
		var a = abs.getInt(variable,s);
		callback(params,a)

	});
};

/**
 *
 * @Get the return value from the abus bridge
 * @param {String} the returned variable name
 * @param {V8::Pointer} the json pointer
 * @return {Number}
 */
Abus.prototype.getInt= function(variable,dataPortlet){
	if(dataPortlet){
		return this.proxy.get_int(dataPortlet,variable);
	}
	else
  	return this.proxy.get_int(this.jsonResult,variable);
};


/**
 *
 * @Get the return value from the abus bridge
 * @param {String} the returned variable name
 * @param {V8::Pointer} the json pointer
 * @return {Number}
 */
 
Abus.prototype.getLong= function(variable,dataPortlet){
	if(dataPortlet){
		return this.proxy.get_llint(dataPortlet,variable);
	}
	else
  	return this.proxy.get_llint(this.jsonResult,variable);
};

/**
 *
 * @Get the return value from the abus bridge
 * @param {String} the returned variable name
 * @param {V8::Pointer}
 * @return {Number}
 */
Abus.prototype.getDouble= function(variable,dataPortlet){
	if(dataPortlet)
		return this.proxy.get_double(dataPortlet,variable);
	else
  	return this.proxy.get_double(this.jsonResult,variable);
};

/**
 *
 * @Get the return value from the abus bridge
 * @param {String} the returned variable name
 * @param {V8::Pointer}
 * @return {Boolean}
 */
Abus.prototype.getBoolean= function(variable,dataPortlet){
	if(dataPortlet)
  	return this.proxy.get_bool(dataPortlet,variable);
	else
  	return this.proxy.get_bool(this.jsonResult,variable);
};

/**
 *
 * @Get the return value from the abus bridge
 * @param {String} the returned variable name
 * @param {V8::Pointer} the returned variable name
 * @return {String}
 */
Abus.prototype.getString= function(variable,length,dataPortlet){
	if(dataPortlet)
  	return this.proxy.get_string(dataPortlet,variable,length);
  else
		return this.proxy.get_string(this.jsonResult,variable,length);
};


/**
 *
 * @Get the return value from the abus bridge
 * @param {String} the returned variable name
 * @param {String} the format to return
 * @return {Number}
 */
Abus.prototype.get= function(type,variable){
	this.jsonResult = this.proxy.invoke(this.dataPortlet,0,10)
	if(type == 'i')
  	return this.proxy.get_int(this.jsonResult,variable);
	else if(type == 'd')
  	return this.proxy.get_double(this.jsonResult,variable);
	else if(type == 's')
  	return this.proxy.get_string(this.jsonResult,variable);
	else if(type == 'b')
  	return this.proxy.get_bool(this.jsonResult,variable);
};

/**
 * @param {String} eventName the event that want to register on
 * @param {Function} callback the executed callback when the event dispatched
 * @param {String | Object} service : it can be local service by passing the string name or remote service by passing the object desscriptor {ip: '202.0.3.4',port: 5000,serviceName: 'calculator', protocol: 'udp/tcp'}
 *
 */
Abus.prototype.addEventListener= function(eventName,callback,service){
	if(service !=  undefined){
		if(typeof service != 'string' && service.ip != undefined && service.ip != '' && service.port != undefined  && service.serviceName!= undefined && service.serviceName != '' && service.protocol != undefined && service.protocol != ''){
			service = service.protocol+'://'+service.ip+':'+service.port+'@'+service.serviceName;
		}
	}else{
		service = this.getServiceURI();
	}

	this.proxy.subscribeEvent(service,eventName,0,1000,callback);
};


/**
 * @param {String} eventName the event that want to remove
 *
 */
Abus.prototype.removeEventListener= function(service,eventName){
	this.proxy.unsubscribeEvent(service,eventName);
	this.registredEvents[eventName] = undefined;
	delete this.registredEvents[eventName];
};


/**
 * Register an event on nodeservice
 * @param {String} eventName the name of the event that will be dispatched
 * @param {String} description of the event that will be dispatched
 * @param {Array}  returns data to fire with the event
 */
Abus.prototype.addEventDispatcher= function(eventName,descr,returns){
	this.registredEvents[eventName]= {eventName:eventName,descr: descr,returns: returns};
	this.proxy.decl_event(eventName,descr,returns);
};

/**
 * dispatchEvent : it's a dynamic function that cast and prepare the data for dispatching
 * @param {String} eventName
 * ... other params
 *
 */
Abus.prototype.dispatchEvent= function(){
	var eventName= arguments[0];
	var dataPortlet = this.proxy.event_init(this.localServiceName,eventName);

	var argsMetadata= this.registredEvents[eventName].returns;
	args= argsMetadata.split(',');

	for(var i=0;i< args.length;i++){
		var defs = args[i].split(':');
		this.append(defs[0],arguments[i+1],defs[1],dataPortlet);
	}

	this.proxy.publish(dataPortlet);
	this.proxy.event_cleanup(dataPortlet);
};

/**
 * @param {String} the name of the method
 * @param {String} the parameters descriptor
 * @param {String} the returns descriptor
 *
 */
Abus.prototype.addMethod= function(methodName,params,returns,callback){
	this.proxy.decl_method(methodName,params,returns,0,callback);
	this.registredMethods[methodName] = {methodName: methodName,params: params, returns: returns};
};

/**
 * @param {String} the name of the method
 * @param {String} the parameters descriptor
 * @param {String} the returns descriptor
 *
 */
Abus.prototype.removeMethod= function(methodName){
	this.proxy.undecl_method(methodName);
	this.registredMethods[methodName] = undefined;
	delete this.registredMethods[methodName];
};


/**
 * Export the Abus Class
 */
module.exports = Abus;
